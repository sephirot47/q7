# -*- coding: utf-8 -*-
from Crypto.Cipher import AES

# Tells if the padding is correct for the passed decrypted text
def isGoodDecryption(decryptedText):
	lastByte = decryptedText[-1] # Get padding
	if (lastByte <= 1): 
		print ("We cant know if its good or bad...")
		return False
	patternMatch = bytes([lastByte] * lastByte)
	for i in range(lastByte):
		x1 = str(decryptedText[-lastByte+i])
		x2 = str(patternMatch[i])
		if (x1 != x2): return False
	return True

def main():
	# Read key
	fKey = open("2016_10_10_16_59_09_victor.anton.key", "rb")
	strKey = fKey.read()

	# Read encrypted message
	fEncrypted = open("2016_10_10_16_59_09_victor.anton.enc", "rb")
	strEncrypted = fEncrypted.read() 
	iv = strEncrypted[:16] # Get IV
	strEncrypted = strEncrypted[16:]

	# Create AES Object
	for mode in [AES.MODE_ECB, AES.MODE_CBC, AES.MODE_OFB, AES.MODE_CBC, AES.MODE_CFB]:
		aes = AES.new(strKey, mode, iv)  # Create AES object
		decryptedText = aes.decrypt(strEncrypted) # Decrypt
		if (isGoodDecryption(decryptedText)):
			print("Good decryption found with mode: ", mode) 
			break	
		# Get which message is good

	print ("Saving result...")
	fResult = open("resultEx1.mp4", "wb")
	print ("Result left in 'resultEx1.mp4'")
	fResult.write(decryptedText)

if __name__ == "__main__":
	main()
