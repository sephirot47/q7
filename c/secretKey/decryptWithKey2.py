# -*- coding: utf-8 -*-
from Crypto.Cipher import AES
from decryptWithKey import isGoodDecryption

def main(): 
	# Read key
	fEncrypted = open("2016_10_10_16_59_09_victor.anton.puerta_trasera.enc", "rb")
	strEncrypted = fEncrypted.read()
	IV = strEncrypted[:16] # Get IV
	strEncrypted = strEncrypted[16:]

	for kiv in range(0,256):
		print ("Trying with kiv:", kiv)

		KS = [0] * 16
		for i in range(len(KS)): KS[i] = IV[i] ^ kiv
		KS = bytes(KS)

		aes = AES.new(KS, AES.MODE_CBC, IV)
		decryptedText = aes.decrypt(strEncrypted) # Decrypt

		if (isGoodDecryption(decryptedText)):
			print ("Found a good decryption with kiv:", kiv)
			break


	print ("Saving result...")
	fResult = open("resultEx2.mp4", "wb")
	print ("Result left in 'resultEx2.mp4'")
	fResult.write(decryptedText)

if __name__ == "__main__":
        main()
