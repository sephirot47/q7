import sys
import time
import string
import numpy as np
	
MOD_MASK   = 0b100011011

def shiftWithMod(a):
	return ((a << 1) ^ MOD_MASK) if ((a << 1) & 0x100) else a << 1

# NORMAL PRODUCT
def GF_product_p(a, b):
	r = 0
	while (b != 0):
		if (b & 1) != 0: r ^= a
		a = shiftWithMod(a)
		b >>= 1 
	return r

# GENERATE TABLES
def GF_tables():
	expTable = [0x01]
	logTable = [0] * 256
	for i in range(0,256):
		expTable.append( GF_product_p(expTable[-1], 0x03) )
		logTable[ expTable[i] ] = i
	return expTable, logTable
expt, logt = GF_tables()

# TABLE PRODUCT
def GF_product_t(a, b):
	return expt[(logt[a] + logt[b]) % 255]

# OPT 0x02
def GF_product_p_02(a):
	return shiftWithMod(a)
def GF_product_t_02(a):
	return expt[(logt[a] + 0x19) % 255]

# OPT 0x03
def GF_product_p_03(a):
	return a ^ shiftWithMod(a)
def GF_product_t_03(a):
	return expt[(logt[a] + 0x01) % 255]

# OPT 0x09
def GF_product_p_09(a):
	b = shiftWithMod(a)
	b = shiftWithMod(b)
	b = shiftWithMod(b)
	return a ^ b
def GF_product_t_09(a):
	return expt[(logt[a] + 0xc7) % 255]

# OPT 0x0B
def GF_product_p_0B(a):
	b = shiftWithMod(a)
	c = shiftWithMod(b)
	c = shiftWithMod(c)
	return a ^ b ^ c
def GF_product_t_0B(a):
	return expt[(logt[a] + 0x68) % 255]

# OPT 0x0D
def GF_product_p_0D(a):
	b = shiftWithMod(a)
	b = shiftWithMod(b)
	c = shiftWithMod(b)
	return a ^ b ^ c
def GF_product_t_0D(a):
	return expt[(logt[a] + 0xee) % 255]

# OPT 0x0E
def GF_product_p_0E(a):
	a = shiftWithMod(a)
	b = shiftWithMod(a)
	c = shiftWithMod(b)
	return a ^ b ^ c
def GF_product_t_0E(a):
	return expt[(logt[a] + 0xdf) % 255]

# GENERATORS
def GF_generador():
	generators = []
	for i in range(0, 256):
		x = i
		found = set()
		for _ in range(0, 255):
			x = GF_product_t(x, i)
			if x in found: break
			found.add(x)
		if len(found) == 255:
			generators.append(x)
	return generators

# INVERS
def GF_invers(a):
	return expt[255-logt[a]]

#  COMPARATIVES
def runProduct(functionTemplate, numTests):
	if ("Y" in functionTemplate):
		for i in range(numTests):
			for a in range(255):
				for b in range(255):
					eval(functionTemplate.replace("X", str(a)).replace("Y", str(b)))
	else:
		for i in range(numTests):
			for a in range(255):
				eval(functionTemplate.replace("X", str(a)))

def compareFunctions(functionTemplate1, functionTemplate2, numTests):
	print("Fent comparativa de", functionTemplate1, "vs", functionTemplate2, "...")
	time1 = time.time()
	runProduct(functionTemplate1, numTests)
	time1 = time.time() - time1
	print("Temps de", functionTemplate1, ": ", time1)

	time2 = time.time()
	runProduct(functionTemplate2, numTests)
	time2 = time.time() - time2
	print("Temps de", functionTemplate2, ": ", time2)

	print(functionTemplate2, "es", (time1 / time2), "vegades mes rapid que", functionTemplate1)
	print("-----------------------------------\n")

'''
compareFunctions("GF_product_p(X,Y)", "GF_product_t(X,Y)", 2)
numTests = 300
compareFunctions("GF_product_p_02(X)",    "GF_product_t_02(X)", numTests)
compareFunctions("GF_product_p_03(X)",    "GF_product_t_03(X)", numTests)
compareFunctions("GF_product_p_09(X)",    "GF_product_t_09(X)", numTests)
compareFunctions("GF_product_p_0B(X)",    "GF_product_t_0B(X)", numTests)
compareFunctions("GF_product_p_0D(X)",    "GF_product_t_0D(X)", numTests)
compareFunctions("GF_product_p_0E(X)",    "GF_product_t_0E(X)", numTests)
compareFunctions("GF_product_p(X,0x02)",  "GF_product_p_02(X)", numTests)
compareFunctions("GF_product_p(X,0x03)",  "GF_product_p_03(X)", numTests)
compareFunctions("GF_product_p(X,0x09)",  "GF_product_p_09(X)", numTests)
compareFunctions("GF_product_p(X,0x0B)",  "GF_product_p_0B(X)", numTests)
compareFunctions("GF_product_p(X,0x0D)",  "GF_product_p_0D(X)", numTests)
compareFunctions("GF_product_p(X,0x0E)",  "GF_product_p_0E(X)", numTests)
compareFunctions("GF_product_t(X, 0x02)", "GF_product_t_02(X)", numTests)
compareFunctions("GF_product_t(X, 0x03)", "GF_product_t_03(X)", numTests)
compareFunctions("GF_product_t(X, 0x09)", "GF_product_t_09(X)", numTests)
compareFunctions("GF_product_t(X, 0x0B)", "GF_product_t_0B(X)", numTests)
compareFunctions("GF_product_t(X, 0x0D)", "GF_product_t_0D(X)", numTests)
compareFunctions("GF_product_t(X, 0x0E)", "GF_product_t_0E(X)", numTests)
'''

# Tests privats #################

# Test to see if optimized versions give the correct result
'''
ok = True
for i in range(1,255):
	if (GF_product_p(i, 0x02) != GF_product_p_02(i)): ok = False; break
print ("p_02: ", ok)
ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x02) != GF_product_t_02(i)): ok = False; break
print ("t_02: ", ok)

ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x03) != GF_product_p_03(i)): ok = False; break
print ("p_03: ", ok)
ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x03) != GF_product_t_03(i)): ok = False; break
print ("t_03: ", ok)

ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x09) != GF_product_p_09(i)): ok = False; break
print ("p_09: ", ok)
ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x09) != GF_product_t_09(i)): ok = False; break
print ("t_09: ", ok)

ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x0B) != GF_product_p_0B(i)): ok = False; break
print ("p_0B: ", ok)
ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x0B) != GF_product_t_0B(i)): ok = False; break
print ("t_0B: ", ok)

ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x0D) != GF_product_p_0D(i)): ok = False; break
print ("p_0D: ", ok)
ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x0D) != GF_product_t_0D(i)): ok = False; break
print ("t_0D: ", ok)

ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x0E) != GF_product_p_0E(i)): ok = False; break
print ("p_0E: ", ok)
ok = True
for i in range(1,255):
	 if (GF_product_p(i, 0x0E) != GF_product_t_0E(i)): ok = False; break
print ("t_0E: ", ok)
'''

# Test to see the tables
print("\nExponential:"); print(expt) # for i in range(0, len(i)): print(i, ":", expt[i])
print("\nLogarithm:");   print(logt) # for i in range(0, len(i)): print(i, ":", logt[i])
inv = [GF_invers(i) for i in range(0,256)]
print ("\nInverses:");   print(inv)

# Test to see the 128 generators
print("\nGenerators:");  print(GF_generador())


print (hex(GF_product_p(0x0b,0x1C)))
print("----------------------------------")
#print (hex(GF_product_p(0x03, 0xf0)))
print (hex(GF_product_p(0xF1,0x1C)))
print (hex(GF_product_p(0xF8,0x1C)))
print (hex(GF_product_p(0x7c,0x1C)))

