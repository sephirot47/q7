import sys
import string
import numpy as np

f = open("2016_09_12_19_00_45_victor.anton.Cesar", 'r')
contents = f.read()

for k in range(0,25):
	print "K:", k, "----------------------------"
	msg = ""
	for c in contents:
		x = c
		if ord(c) >= ord('A') and ord(c) <= ord('Z'):
			x = chr( (ord(c) - ord('A') + k) % 26 + ord('A') )
		if ord(c) >= ord('a') and ord(c) <= ord('z'):
			x = chr( (ord(c) - ord('a') + k) % 26 + ord('a') )
		msg += x
	print msg 
	print "-------------------------------------"
	print ; print

