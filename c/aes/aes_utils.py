import random

def bytesToString(bytesList):
	result = ""
	for b in bytesList:
		bstr = str(hex(b)).replace("0x", "")
		if (len(bstr) == 1): bstr = '0' + bstr
		result += bstr + ":"
	return result[0:-1]

def bytesToRealString(bytesList):
        return "".join([chr(b) for b in bytesList])

def toBits(string):
        return  "".join(_toBits(x) for x in string)

def _toBits(character):
        strbin = str(bin(character))
        strbin = strbin[2:] # Remove 0b
        for i in range(len(strbin), 8): # Add padding until 8 bits
                strbin = '0' + strbin
        return strbin

def countDifferentBits(str1, str2):
        sb1 = toBits(str1)
        sb2 = toBits(str2)
        diff = 0
        for i in range(0, min(len(sb1), len(sb2))):
                if (sb1[i] != sb2[i]):
                        diff += 1
        return diff


def getDifferentBitPos(str1, str2):
        sb1 = toBits(str1)
        sb2 = toBits(str2)
        differentBitPos = []
        for i in range(0, min(len(sb1), len(sb2))):
                if (sb1[i] != sb2[i]):
                        differentBitPos.append(i)
        return differentBitPos

def changeOneRandomBit(slist):
	result = [b for b in slist]
	i = random.randrange(0, len(slist))
	b = slist[i]
	exponent = random.randrange(0,8)
	xorMask = 2**exponent
	b ^= xorMask
	result[i] = b
	return result

def changeOneRandomBitString(string):
        bytes = [ord(c) for c in string]
        bytes = changeOneRandomBit(bytes)
        return "".join( [chr(b) for b in bytes] )

def xor(M1, M2):
        Mxor = []
        for i in range(0, len(M1)):
                Mxor.append( M1[i] ^ M2[i] )
        return Mxor

def xorString(M1, M2):
        Mxor = ""
        for i in range(0, len(M1)):
                Mxor += chr(ord(M1[i]) ^ ord(M2[i]))
        return Mxor

def getRandomBlock():
        M = []
        for i in range(0,16):
                M.append( random.randrange(0,255) )
        return M

def getRandomMessage():
        M = ""
        for i in range(0,16):
                M += chr( random.randrange(0,255) )
        return M

