import aes
import aes_utils as au

aes.USE_SUBBYTES_IDENTITY  = False; aes.USE_SHIFTROWS_IDENTITY = False; aes.USE_MIXCOLS_IDENTITY   = False

k  = au.getRandomMessage()
M  = au.getRandomMessage()
Mi = au.changeOneRandomBitString(M)

print("\n--------------------------------------\nMessages:")
print("k:   " + ':'.join(x.encode('hex') for x in k))
print("M:   " + ':'.join(x.encode('hex') for x in M))
print("Mi:  " + ':'.join(x.encode('hex') for x in Mi))

print("\n-----------------------------------------\nResultados: ") 
aes.USE_SHIFTROWS_IDENTITY = True
C  = aes.encryptData(k, M)
Ci = aes.encryptData(k, Mi)

print("Si nos fijamos, las si shiftRows es la identidad, solo 1 fila de la matriz C y Ci es diferente. El resto son iguales.")
print("C:  " + ':'.join(x.encode('hex') for x in C))
print("Ci: " + ':'.join(x.encode('hex') for x in Ci))
print("\n-----------------------------------------\n")


