import aes
import aes_utils as au

aes.USE_SUBBYTES_IDENTITY  = False; aes.USE_SHIFTROWS_IDENTITY = False; aes.USE_MIXCOLS_IDENTITY   = False

k  = au.getRandomMessage()
M  = au.getRandomMessage()
Mi = au.changeOneRandomBitString(M)

print("\n--------------------------------------\nMessages:")
print("k:   " + ':'.join(x.encode('hex') for x in k))
print("M:   " + ':'.join(x.encode('hex') for x in M))
print("Mi:  " + ':'.join(x.encode('hex') for x in Mi))

print("\n-----------------------------------------\nResultados: ")
aes.USE_MIXCOLS_IDENTITY = True
C  = aes.encryptData(k, M)
Ci = aes.encryptData(k, Mi)

print("Si nos fijamos, si mixColumns es la identidad, solo varia un byte de C a Ci. El resto permanecen iguales.")
print("C:  " + ':'.join(x.encode('hex') for x in C))
print("Ci: " + ':'.join(x.encode('hex') for x in Ci))
print("\n-----------------------------------------\n")
