import aes
import aes_utils as au

aes.USE_SUBBYTES_IDENTITY  = False ; aes.USE_SHIFTROWS_IDENTITY = False; aes.USE_MIXCOLS_IDENTITY   = False

k   = au.getRandomMessage()
M = "".join([chr(x) for x in ([0x00] * 16)]);
Mi = au.changeOneRandomBitString(M)
Mj = au.changeOneRandomBitString(M) ; 
while (Mi == Mj): Mj = au.changeOneRandomBitString(M)
Mk = au.changeOneRandomBitString(M) ; 
while (Mi == Mk or Mj == Mk): Mk = au.changeOneRandomBitString(M)
Ml = au.changeOneRandomBitString(M) ; 
while (Mi == Ml or Mj == Ml or Mk == Ml): Ml = au.changeOneRandomBitString(M)
Mijkl = au.xorString(Mi, au.xorString(Mj, au.xorString(Mk, Ml) ) )

print("\n--------------------------------------\nMessages:")
print("k:     " + ':'.join(x.encode('hex') for x in k))
print("M:     " + ':'.join(x.encode('hex') for x in M))
print("Mi:    " + ':'.join(x.encode('hex') for x in Mi))
print("Mj:    " + ':'.join(x.encode('hex') for x in Mj))
print("Mk:    " + ':'.join(x.encode('hex') for x in Mk))
print("Ml:    " + ':'.join(x.encode('hex') for x in Ml))
print("Mijkl: " + ':'.join(x.encode('hex') for x in Mijkl))

print("\n-----------------------------------------\nIdentity BytesSub: ") 
aes.USE_SUBBYTES_IDENTITY = True
C   = aes.encryptData(k, M)
Ci  = aes.encryptData(k, Mi)
Cj  = aes.encryptData(k, Mj)
Ck  = aes.encryptData(k, Mk)
Cl  = aes.encryptData(k, Ml)
print("C :    " + ':'.join(x.encode('hex') for x in C))
print("Ci:    " + ':'.join(x.encode('hex') for x in Ci))
print("Cj:    " + ':'.join(x.encode('hex') for x in Cj))
print("Ck:    " + ':'.join(x.encode('hex') for x in Ck))
print("Cl:    " + ':'.join(x.encode('hex') for x in Cl))
Cijkl  = au.xorString(C, au.xorString(Ci, au.xorString(Cj, au.xorString(Ck, Cl))))
print("C ^ Ci ^ Cj ^ Ck ^ Cl: " + ':'.join(x.encode('hex') for x in Cijkl))
Cijkl  = aes.encryptData(k, Mijkl)
print("Cijkl:                 " + ':'.join(x.encode('hex') for x in Cijkl))
print("\n-----------------------------------------\n")


