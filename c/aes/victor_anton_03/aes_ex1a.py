import aes
import aes_utils as au

aes.USE_SUBBYTES_IDENTITY  = False ; aes.USE_SHIFTROWS_IDENTITY = False; aes.USE_MIXCOLS_IDENTITY   = False

k   = au.getRandomMessage()
'''
M   = au.getRandomMessage()
Mi  = au.changeOneRandomBitString(M)
Mj  = au.changeOneRandomBitString(M)
'''
M = "".join([chr(x) for x in ([0x00] * 16)]);
Mi = au.changeOneRandomBitString(M)
Mj = au.changeOneRandomBitString(M)
while (Mi == Mj): Mj = au.changeOneRandomBitString(M)
Mij = au.xorString(M, au.xorString(Mi, Mj))

print("\n--------------------------------------\nMessages:")
print("k:   " + ':'.join(x.encode('hex') for x in k))
print("M:   " + ':'.join(x.encode('hex') for x in M))
print("Mi:  " + ':'.join(x.encode('hex') for x in Mi))
print("Mj:  " + ':'.join(x.encode('hex') for x in Mj))
print("Mij: " + ':'.join(x.encode('hex') for x in Mij))


print("\n-----------------------------------------\nOriginal BytesSub: ") 
aes.USE_SUBBYTES_IDENTITY = False
C   = aes.encryptData(k, M)
Ci  = aes.encryptData(k, Mi)
Cj  = aes.encryptData(k, Mj)
Cij = aes.encryptData(k, Mij)
print("Ci:  " + ':'.join(x.encode('hex') for x in Ci))
print("Cj:  " + ':'.join(x.encode('hex') for x in Cj))
print("Cij: " + ':'.join(x.encode('hex') for x in Cij))

print("\nCon el AES original, C NO tiene porque ser =  Ci ^ Cj ^ Cij")
print("C:             " + ':'.join(x.encode('hex') for x in C))
print("Ci ^ Cj ^ Cij: " + ":".join(x.encode('hex') for x in au.xorString(Ci, au.xorString(Cj, Cij))))

print("\n-----------------------------------------\nIdentity BytesSub: ") 
aes.USE_SUBBYTES_IDENTITY = True
C   = aes.encryptData(k, M)
Ci  = aes.encryptData(k, Mi)
Cj  = aes.encryptData(k, Mj)
Cij = aes.encryptData(k, Mij)
print("Ci:  " + ':'.join(x.encode('hex') for x in Ci))
print("Cj:  " + ':'.join(x.encode('hex') for x in Cj))
print("Cij: " + ':'.join(x.encode('hex') for x in Cij))

print("\nComo he cambiado BytesSub por la identidad, C = Ci ^ Cj ^ Cij")
print("C:             " + ':'.join(x.encode('hex') for x in C))
print("Ci ^ Cj ^ Cij: " + ":".join(x.encode('hex') for x in au.xorString(Ci, au.xorString(Cj, Cij))))
print("\n-----------------------------------------\n")


