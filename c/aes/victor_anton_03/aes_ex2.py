import aes
import matplotlib.pyplot as plt
import aes_utils as au


aes.USE_SUBBYTES_IDENTITY  = False
aes.USE_SHIFTROWS_IDENTITY = False
aes.USE_MIXCOLS_IDENTITY   = False

NUM_TESTS = 100
C_SIZE = 16

# Modifying a bit of M
histNum = {k: 0 for k in range(0, C_SIZE*8)}
histPos = {k: 0 for k in range(0, C_SIZE*8)}
for i in range(0, NUM_TESTS):
	k = au.getRandomBlock()
	M = au.getRandomBlock()
	C  = aes.AES().encrypt(k, M, C_SIZE)
	Mi = au.changeOneRandomBit(M)
        Ci = aes.AES().encrypt(k, Mi, C_SIZE)
        numDiffBits = au.countDifferentBits(C, Ci)
        histNum[numDiffBits] += 1
        for diffBitPos in au.getDifferentBitPos(C,Ci):
            histPos[diffBitPos] += 1
plt.bar(histNum.keys(), histNum.values()) ; plt.axis([0, 128, 0, max(histNum.values())]); plt.show()
plt.bar(histPos.keys(), histPos.values()) ; plt.axis([0, 128, 0, max(histPos.values())]); plt.show()

# Modifying a bit of K
histNum = {k: 0 for k in range(0, C_SIZE*8)}
histPos = {k: 0 for k in range(0, C_SIZE*8)}
for i in range(0, NUM_TESTS):
	k = au.getRandomBlock()
	M = au.getRandomBlock()
	C  = aes.AES().encrypt(k, M, C_SIZE)
	ki = au.changeOneRandomBit(k)
        Ci = aes.AES().encrypt(ki, M, C_SIZE)
        numDiffBits = au.countDifferentBits(C, Ci)
        histNum[numDiffBits] += 1
        for diffBitPos in au.getDifferentBitPos(C,Ci):
            histPos[diffBitPos] += 1
plt.bar(histNum.keys(), histNum.values()); plt.axis([0, 128, 0, max(histNum.values())]); plt.show()
plt.bar(histPos.keys(), histPos.values()); plt.axis([0, 128, 0, max(histPos.values())]); plt.show()


