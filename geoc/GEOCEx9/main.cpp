#include <cmath>
#include <vector>
#include <iostream>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#define PI 3.141592f

const int windowWidth  = 800;
const int windowHeight = 600;

std::vector<sf::Vector2i> points;

int getSide(const sf::Vector2i &lineP1, const sf::Vector2i &lineP2,
            const sf::Vector2i &p)
{
    float orientation = (lineP2.x - lineP1.x) * (p.y - lineP1.y) -
                        (p.x - lineP1.x) * (lineP2.y - lineP1.y);
    return orientation > 0 ? 1 : ((orientation < 0) ? -1 : 0);
}

float getAngle(const sf::Vector2i &v1, const sf::Vector2i &v2)
{
    float angle = std::abs(std::atan2(v2.y, v2.x) - std::atan2(v1.y, v1.x));
    return std::min(angle, 2 * PI - angle);
}

sf::Color getPointColor(const sf::Vector2i &q)
{
    float angle = 0.0f;
    sf::Vector2i rightRefPoint = points.front(), leftRefPoint = rightRefPoint;
    for (int i = 0; i < points.size(); ++i)
    {
        if (getSide(q, rightRefPoint, points[i]) == 1)
        {   // On the right of the rightRefAxis
            angle += getAngle(rightRefPoint-q, points[i]-q);
            rightRefPoint = points[i];
        }
        else if (getSide(q, leftRefPoint, points[i]) == -1)
        {   // On the left of the leftRefAxis
            angle += getAngle(leftRefPoint-q, points[i]-q);
            leftRefPoint = points[i];
        }
    }
    if (std::abs(angle - PI) < 0.02f) return sf::Color::Blue;
    else if(angle < PI) return sf::Color::Red;
    return sf::Color::Green;
}

int main()
{
    const sf::Color colorExistingPoint = sf::Color::White;

    sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "SFML window");

    sf::Image buffer;
    buffer.create(windowWidth, windowHeight, sf::Color(0, 0, 0));

    sf::Texture tex;
    sf::Sprite sprite;


    std::cout << getAngle(sf::Vector2i(1,0), sf::Vector2i(0,1)) << std::endl;

    while (window.isOpen())
    {
        sf::Event event;
        window.clear();

        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
            else if (event.type == sf::Event::MouseButtonPressed)
            {
                sf::Vector2i mousePos = sf::Mouse::getPosition(window);
                if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
                {
                    points.push_back(mousePos);
                    buffer.setPixel(mousePos.x, mousePos.y, colorExistingPoint);
                }
                else if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
                {
                    sf::Color color = getPointColor(mousePos);
                    buffer.setPixel(mousePos.x, mousePos.y, color);
                }
            }
            else if (event.type == sf::Event::KeyPressed)
            {
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
                {
                    points.clear();
                    buffer.create(windowWidth, windowHeight, sf::Color(0, 0, 0));
                }
                else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
                {
                    for (int i = 0; i < 999999; ++i)
                    {
                        sf::Vector2i p(rand() % windowWidth, rand() % windowHeight);
                        sf::Color color = getPointColor(p);
                        buffer.setPixel(p.x, p.y, color);
                    }
                }
            }
        }

        tex.loadFromImage(buffer);
        sprite.setTexture(tex);
        window.draw(sprite);
        window.display();
    }

    return EXIT_SUCCESS;
}
