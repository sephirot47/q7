TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

LIBS += -lsfml-graphics \
	-lsfml-window \
	-lsfml-system

CONFIG += c++0x
