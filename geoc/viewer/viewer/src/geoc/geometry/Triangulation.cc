#include <geoc/geometry/Triangulation.h>
#include <geoc/geometry/Triangle.h>
#include <geoc/geometry/Circle.h>
#include <geoc/math/Math.h>
#include <geoc/scene/LineSegmentEnt.h>
#include <geoc/scene/TriangleEnt.h>
#include <geoc/GeocException.h>
#include <geoc/geometry/CgalTriangulation.h>
#include <algorithm>
#include <sstream>
#include <cstdio>


using namespace geoc;
using namespace std;


Triangulation::Triangulation()
{
}


Triangulation::~Triangulation()
{
}

bool Triangulation::OutsideBoundaries(const std::vector< std::vector<int> > &boundariesIndices,
                                     const std::vector<Vector3>& points,
                                     const Vector3 &p) const
{
    for (int j = 0; j < boundariesIndices.size(); ++j)
    {
        const std::vector<int> &boundaryIndices = boundariesIndices[j];
        for (int i = 0; i < boundaryIndices.size(); ++i)
        {
            const Vector2 &boundaryPoint = points[ boundaryIndices[i] ];
            if (boundaryPoint == p)
            {
                return true;
            }
        }
    }
    return false;
}

bool Triangulation::OutsideBoundaries(const std::vector< std::vector<int> > &boundariesIndices,
                                     const std::vector<Vector3>& points,
                                     const Triangle &tri) const
{
    return OutsideBoundaries(boundariesIndices, points, tri[0]) &&
           OutsideBoundaries(boundariesIndices, points, tri[1]) &&
           OutsideBoundaries(boundariesIndices, points, tri[2]);
}

double Triangulation::Dot(const Vector2 &v1, const Vector2 &v2) const
{
    return v1.x() * v2.x() + v1.y() * v2.y() + v1.z() * v2.z();
}

void Triangulation::triangulate(const std::vector<Vector3>& _points,
                                const std::vector<int>& idxs,
                                std::vector<LineSegmentEnt>& segments,
                                std::vector<TriangleEnt>& triangles,
                                std::vector<TriangleEnt>& triangles_pruned)
{
    //CgalTriangulation ct; ct.triangulate(points, idxs, segments, triangles, triangles_pruned); return;

    printf("Compiling student triangulation\n");
    std::vector<Vector3> points = _points;
    num minX = points[0][X], maxX = points[0][X];
    num minY = points[0][Y], maxY = points[0][Y];
    int i = 0;
    for (const Vector3 &p : points)
    {
        minX = std::min(minX, p[X]); maxX = std::max(maxX, p[X]);
        minY = std::min(minY, p[Y]); maxY = std::max(maxY, p[Y]);
    }
    std::random_shuffle(points.begin(), points.end());

    DCEL polygon(minX, maxX, minY, maxY);
    num bboxWidth = (maxX - minX), bboxHeight = (maxY - minY);
    bboxWidth  = std::max(0.00005, bboxWidth);
    bboxHeight = std::max(0.00005, bboxHeight);
    std::cout << bboxWidth << ", " << bboxHeight << std::endl;

    const num t60 = std::tan(60 * M_PI / 180.0f);

    const num goodLookingOffsetX = bboxWidth  * 0.005f;
    const num goodLookingOffsetY = bboxHeight * 0.005f;
    num superTriMaxX = ( maxX + bboxHeight / t60 )      + goodLookingOffsetX;
    num superTriMinX = ( minX - bboxHeight / t60 )      - goodLookingOffsetX;
    num superTriMaxY = ( maxY + t60 * (bboxWidth / 2) ) + goodLookingOffsetY;
    num superTriMinY = (minY)                           - goodLookingOffsetY;

    polygon.AddFaceTri(Vector2((superTriMaxX + superTriMinX) / 2, superTriMaxY),
                       Vector2(                     superTriMinX, superTriMinY),
                       Vector2(                     superTriMaxX, superTriMinY));

    for (int i = 0; i < points.size(); ++i)
    {
        const Vector3 &p = points[i];
        //if (DEBUG || i % 1000 == 0) std::cout << "Point " << i << ": " << p << std::endl;
        DCEL::Face *tri = polygon.GetContainingFace(p);
        polygon.AddVertexInsideTri(tri, p);
    }

    DCEL::Vertex *superTriV1 = polygon.GetVertex(0);
    DCEL::Vertex *superTriV2 = polygon.GetVertex(1);
    DCEL::Vertex *superTriV3 = polygon.GetVertex(2);

    std::vector< std::vector<int> > boundariesIndices = {{}};
    for (int i = 0; i < idxs.size(); ++i)
    {
        if (idxs[i] == -1) { boundariesIndices.push_back({}); }
        else { boundariesIndices[boundariesIndices.size()-1].push_back(idxs[i]); }
    }

    if (DEBUG) std::cout << std::endl << "Pushing triangles...." << i << std::endl;
    for (const DCEL::Face *triFace : polygon.faces)
    {
        if (DEBUG) std::cout << "Triangle: " << triFace->ToString() << " " << (triFace->valid ? "VALID" : "INVALID") << std::endl;
        if (!triFace->valid) continue;

        Triangle tri = triFace->GetTriangle3d();

        DCEL::Vertex *v1 = triFace->boundary->origin;
        DCEL::Vertex *v2 = triFace->boundary->next->origin;
        DCEL::Vertex *v3 = triFace->boundary->next->next->origin;
        if (v1 != superTriV1 && v2 != superTriV1 && v3 != superTriV1 &&
            v1 != superTriV2 && v2 != superTriV2 && v3 != superTriV2 &&
            v1 != superTriV3 && v2 != superTriV3 && v3 != superTriV3)
        {
            if (!OutsideBoundaries(boundariesIndices, _points, tri))
            {
                triangles_pruned.push_back(tri);
            }
        }

        triangles.push_back(tri);
    }
}
