#ifndef _GEOC_TRIANGULATION_H
#define _GEOC_TRIANGULATION_H

#include <geoc/math/Math.h>
#include <geoc/geoc.h>
#include <geoc/geometry/LineSegment.h>
#include <geoc/geometry/TriangulationBase.h>
#include <geoc/math/Vector_fwd_decl.h>
#include <geoc/scene/TriangleEnt.h>
#include <vector>
#include <list>

#define DEBUG false

namespace geoc {

class LineSegmentEnt;
class TriangleEnt;
class Triangle;

class DCEL
{
public:
    class Vertex;
    class HalfEdge;
    class Face;

    DCEL(float minx, float maxx,
         float miny, float maxy)
    {
        xSize = (maxx - minx) * 1.05f;
        ySize = (maxy - miny) * 1.05f;
        minX = minx;
        minY = miny;
    }

    class IDCELElement
    {
    public:
        bool valid = true;
    };

    class Vertex : public IDCELElement
    {
    public:
        HalfEdge *incidentEdge = nullptr;
        Vector2 coord;
        float z = 0.0f;

        Vertex(const Vector3 &coord)
        {
            this->coord = Vector2(coord.x(), coord.y());
            this->z = coord.z() * 5.0f;
        }

        Vector3 GetCoord3d()
        {
            return Vector3(coord.x(), coord.y(), z);
        }
    };

    class HalfEdge : public IDCELElement
    {
    public:
        Vertex *origin = nullptr;
        HalfEdge *twin = nullptr;
        HalfEdge *next = nullptr;
        Face *face = nullptr;

        HalfEdge() {}
        HalfEdge(Vertex *origin, HalfEdge *twin, HalfEdge *next, Face *face)
        {
            this->origin = origin;
            this->twin = twin;
            this->next = next;
            this->face = face;
        }

        std::string ToString() const
        {
            std::ostringstream oss;
            oss << "(" << GetOriginCoord() << ", " << GetDestinyCoord() << ")";
            return oss.str();
        }

        Vertex* GetOrigin() const { return origin; }
        Vertex* GetDestiny() const { return next->origin; }
        const Vector2& GetOriginCoord() const { return GetOrigin()->coord; }
        const Vector2& GetDestinyCoord() const { return GetDestiny()->coord; }

        Vector2 GetVector() const { return (next->origin->coord - origin->coord); }
        LineSegment GetLineSegment() const { return LineSegment(origin->coord, next->origin->coord); }
        HalfEdge *GetPrev() const { return twin->next; }
    };

    class Face : public IDCELElement
    {
    public:
        HalfEdge *boundary = nullptr;

        Face() {}
        Face(HalfEdge *boundary)
        {
            this->boundary = boundary;
        }

        HalfEdge *GetHalfEdge(int i) const
        {
            HalfEdge *he = this->boundary;
            for (int j = 0; j < i; ++j) he = he->next;
            return he;
        }

        HalfEdge *GetHalfEdgeContaining(const Vector2 &v1, const Vector2 &v2) const
        {
            HalfEdge *he = this->boundary;
            for (int i = 0; i < 3; ++i)
            {
                if ( (he->GetOriginCoord() == v1 && he->GetDestinyCoord() == v2) ||
                     (he->GetOriginCoord() == v2 && he->GetDestinyCoord() == v1))
                {
                    return he;
                }
                he = he->next;
            }
            return nullptr;
        }

        Vertex *GetVertexNotEqualTo(Vertex *v1, Vertex *v2)
        {
            Vertex *tri_v1 = GetVertex(0), *tri_v2 = GetVertex(1), *tri_v3 = GetVertex(2);
            if (tri_v1 != v1 && tri_v1 != v2) { return tri_v1; }
            if (tri_v2 != v1 && tri_v2 != v2) { return tri_v2; }
            if (tri_v3 != v1 && tri_v3 != v2) { return tri_v3; }
            return nullptr;
        }

        Vertex *GetVertex(int i) const
        {
            return GetHalfEdge(i)->origin;
        }

        const Vector2& GetCoord(int i) const
        {
            return GetVertex(i)->coord;
        }

        Vector3 GetCoord3d(int i) const
        {
            return GetVertex(i)->GetCoord3d();
        }

        bool Contains(const Vector2 &p, bool *inBoundary = nullptr, HalfEdge **inBoundaryHalfEdge = nullptr) const
        {
            Vector2 v1 = GetCoord(0);
            const Vector2 &v2 = GetCoord(1);
            Vector2 v3 = GetCoord(2);

            bool ccw = DCEL::Orientation2D(v1, v2, v3) < 0;
            if (!ccw) { std::swap(v1,v3); } // Always in ccw

            int o12 = DCEL::Orientation2D(v1, v2, p);
            int o23 = DCEL::Orientation2D(v2, v3, p);
            int o31 = DCEL::Orientation2D(v3, v1, p);
            if (inBoundaryHalfEdge)
            {
                if      (o12 == 0) { *inBoundaryHalfEdge = GetHalfEdgeContaining(v1, v2); }
                else if (o23 == 0) { *inBoundaryHalfEdge = GetHalfEdgeContaining(v2, v3); }
                else if (o31 == 0) { *inBoundaryHalfEdge = GetHalfEdgeContaining(v3, v1); }
            }
            if (inBoundary) { *inBoundary = (o12 == 0) || (o23 == 0) || (o31 == 0); }
            return o12 <= 0 && o23 <= 0 && o31 <= 0;
        }


        std::string ToString() const
        {
            std::ostringstream oss;
            oss << "F(" << GetCoord(0) << ", " << GetCoord(1) << ", " << GetCoord(2) << ")";
            return oss.str();
        }

        void Print() const
        {
            std::cout << ToString() << std::endl;
        }

        Triangle GetTriangle() const
        {
            return Triangle(GetCoord(0), GetCoord(1), GetCoord(2));
        }

        Triangle GetTriangle3d() const
        {
            return Triangle(GetCoord3d(0), GetCoord3d(1), GetCoord3d(2));
        }
        Triangle GetTriangle3dCCW() const
        {
            return Triangle(GetCoord3d(2), GetCoord3d(1), GetCoord3d(0));
        }
    };

    // Given (p,q,r) colinear points => returns if r is between(p,q).
    static bool between(const Vector3 &p, const Vector3 &q, const Vector3 &r)
    {
        return r[X] >= std::min(p[X], q[X]) && r[X] <= std::max(p[X], q[X]) &&
               r[Y] >= std::min(p[Y], q[Y]) && r[Y] <= std::max(p[Y], q[Y]);
    }

    static int GetIntersectionType(const LineSegment &s, const LineSegment &t)
    {
        Vector3 p0 = s.point(0), p1 = s.point(1);
        Vector3 p2 = t.point(0), p3 = t.point(1);

        // Calculate orientations
        double o012 = Math::orientation2D(p0, p1, p2);
        double o013 = Math::orientation2D(p0, p1, p3);
        double o230 = Math::orientation2D(p2, p3, p0);
        double o231 = Math::orientation2D(p2, p3, p1);

        if (o012 != o013 && o230 != o231)
        {
            if (o012 * o013 * o230 * o231 == 0) { return 1; } // Partial collision
            return 2;
        }
        else if (o012 == 0 && o013 == 0 && o230 == 0 && o231 == 0)
        {
            if (between(p0, p1, p2) || between(p0, p1, p3) ||
                between(p2, p3, p0) || between(p2, p3, p1))
            {
                return 1;
            }
        }
        return 0;
    }

    static bool Intersects(const LineSegment &s, const LineSegment &t)
    {
        return GetIntersectionType(s,t) != 0;
    }

    static num Orientation2D(const Vector2& p, const Vector2& q, const Vector2& r)
    {
        num crossProduct = ((q[X] - p[X]) * (r[Y] - p[Y]) - (q[Y] - p[Y]) * (r[X] - p[X]));
        return crossProduct == 0 ? 0 : ( crossProduct > 0 ? 1 : -1 ) ;
    }

    virtual Face *GetContainingFace(const Vector2 &p)
    {
        Vertex *closeVertex = GetCloseVertexTo(p);

        int iters = 0;
        while (true)
        {
            Face *nextFace = nullptr;
            if (closeVertex == nullptr)
            {
                for (int i = rand() % faces.size(); ; i = (i + 1) % faces.size())
                {
                    Face *f = faces[i];
                    if (f->valid)
                    {
                        nextFace = f;   // Find beginning valid face
                        break;
                    }
                }
            }
            else
            {
                nextFace = closeVertex->incidentEdge->face;
                closeVertex = nullptr;
            }

            while (!nextFace->Contains(p))
            {
                ++iters;
                Vector2 rayOrigin = (nextFace->GetCoord(0) + nextFace->GetCoord(1) + nextFace->GetCoord(2) ) / 3.0f;
                LineSegment ray(rayOrigin, p);
                const HalfEdge* halfEdges[3] = {nextFace->GetHalfEdge(0), nextFace->GetHalfEdge(1), nextFace->GetHalfEdge(2)};
                Face* waysToGo[3];
                int waysToGo_i = 0;
                for (int j = 0; j < 3; ++j)
                {
                    const HalfEdge *he = halfEdges[j];
                    if (Intersects(he->GetLineSegment(), ray))
                    {
                        Face *newWay = (he->face != nextFace) ? he->face : he->twin->face;
                        waysToGo[waysToGo_i++] = (newWay);
                    }
                }
                if (waysToGo_i == 0) { nextFace = nullptr; break; }
                nextFace = waysToGo_i == 1 ? waysToGo[0] : waysToGo[ rand() % waysToGo_i ];
            }
            if (nextFace) { /*std::cout << "iters: " << iters << std::endl;*/ return nextFace; }
        }
    }

    Vertex *GetVertex(int index)
    {
        int i = 0;
        for (Vertex *v : vertices)
        {
            if (i == index) return v;
            ++i;
        }
        return nullptr;
    }

    void FlipEdge(HalfEdge *heToFlip, Face **newFlippedFace1, Face **newFlippedFace2)
    {
        if (DEBUG) std::cout << "FLIP EDGE *************************" << std::endl;
        // Existing edge (v1->v2)
        // New edge to be created (opp1->opp2)
        // heToFlip goes from (v1->v2)
        // heToFlip is in face fopp1

        // Retrieve tons of info
        Vertex *v1 = heToFlip->GetDestiny();
        Vertex *v2 = heToFlip->GetOrigin();
        Face *fopp1 = heToFlip->face, *fopp2 = heToFlip->twin->face;
        if (DEBUG) fopp1->Print();
        if (DEBUG) fopp2->Print();
        Vertex *opp1 = fopp1->GetVertexNotEqualTo(v1,v2);
        Vertex *opp2 = fopp2->GetVertexNotEqualTo(v1,v2);
        HalfEdge *he_v1_to_opp1 = nullptr, *he_opp1_to_v2 = nullptr,
                 *he_v2_to_opp2 = nullptr, *he_opp2_to_v1 = nullptr;
        if (DEBUG) std::cout << v1->coord << ", " << opp1->coord << ", " << v2->coord << ", " << opp2->coord << std::endl;
        Face *fs[2] = {fopp1, fopp2};
        for (Face *fopp : fs)
        {
            for (int i = 0; i < 3; ++i)
            {
                HalfEdge *he = fopp->GetHalfEdge(i);
                //if (!he || !he->twin) { std::cout << "Oops" << std::endl; continue; }
                if (DEBUG) std::cout << "he: (" << he->origin->coord << "->" << he->GetDestinyCoord() << ")" << std::endl;
                if (he->GetOrigin() == v1   && he->GetDestiny() == opp1) he_v1_to_opp1 = he;
                if (he->GetOrigin() == opp1 && he->GetDestiny() ==   v2) he_opp1_to_v2 = he;
                if (he->GetOrigin() == v2   && he->GetDestiny() == opp2) he_v2_to_opp2 = he;
                if (he->GetOrigin() == opp2 && he->GetDestiny() ==   v1) he_opp2_to_v1 = he;
            }
        }
        if (DEBUG) std::cout << "Flipping edge (" << v1->coord << ", " << v2->coord << ") to (" << opp1->coord << ", " << opp2->coord << ")" << std::endl;
      //  int a; std::cin >> a;
        if (!he_v1_to_opp1 || !he_opp1_to_v2 || !he_v2_to_opp2 || !he_opp2_to_v1)
        {
            if (DEBUG) std::cout << "OH FUCK" << std::endl;
            if (DEBUG) std::cout << he_v1_to_opp1 << ", " << he_opp1_to_v2 << ", " << he_v2_to_opp2 << ", " << he_opp2_to_v1 << std::endl;
            return;
        }
        //----------------------------

        // Change everything

        // Incident edges
        he_v1_to_opp1->origin = v1; he_opp1_to_v2->origin = opp1; he_opp2_to_v1->origin = opp2; he_v2_to_opp2->origin = v2;
        heToFlip->origin = opp1; heToFlip->twin->origin = opp2;

        fopp1->boundary = he_v1_to_opp1;
        fopp2->boundary = he_opp1_to_v2;
        he_v1_to_opp1->face = he_opp2_to_v1->face = heToFlip->face = fopp1;
        he_opp1_to_v2->face = he_v2_to_opp2->face = heToFlip->twin->face = fopp2;

        he_v1_to_opp1->next = heToFlip; heToFlip->next = he_opp2_to_v1; he_opp2_to_v1->next = he_v1_to_opp1;
        he_v2_to_opp2->next = heToFlip->twin; heToFlip->twin->next = he_opp1_to_v2; he_opp1_to_v2->next = he_v2_to_opp2;

        *newFlippedFace1 = fopp1;
        *newFlippedFace2 = fopp2;

        if (DEBUG) { std::cout << "Result flipEdge fopp1: "; fopp1->Print(); }
        if (DEBUG) { std::cout << "Result flipEdge fopp2: "; fopp2->Print(); }
    }

    num PointInsideTriangleCircleOrientation(const Vector2 &p, const Triangle &tri)
    {
        // If it's ccw, inverse orientation
        int cwMultiplier = Math::orientation2D(tri[0], tri[1], tri[2]) < 0 ? 1 : -1;
        num orientation = Math::orientation25D(Vector2(tri[0][X], tri[0][Y]),
                                               Vector2(tri[1][X], tri[1][Y]),
                                               Vector2(tri[2][X], tri[2][Y]),
                                               Vector2(p[X], p[Y])) * cwMultiplier;
        return orientation;
    }

    bool PointInsideTriangleCircle(const Vector2 &p, const Triangle &tri)
    {
        return (PointInsideTriangleCircleOrientation(p, tri) <= 0);
    }


    bool AreConcyclic(const Vector2 &p, const Vector2 &q, const Vector2 &r, const Vector2 &t)
    {
        return (PointInsideTriangleCircleOrientation(p, Triangle(t,q,r)) == 0 &&
                PointInsideTriangleCircleOrientation(q, Triangle(p,t,r)) == 0 &&
                PointInsideTriangleCircleOrientation(r, Triangle(p,q,t)) == 0 &&
                PointInsideTriangleCircleOrientation(t, Triangle(p,q,r)) == 0);
    }

    void SplitTriangleInTwo(Face *tri, const Vector3 &splitPoint, Vertex *newVertex,
                            HalfEdge **he_v_col1_ret, HalfEdge **he_col2_v_ret,
                            Face **newTriangle1, Face **newTriangle2)
    {
        if (DEBUG) std::cout << "SplitTriangleInTwo: " << tri->ToString() << std::endl;

        HalfEdge *he_12 = tri->boundary;
        HalfEdge *he_23 = tri->boundary->next;
        HalfEdge *he_31 = tri->boundary->next->next;
        Vertex *v1 = he_12->origin;
        Vertex *v2 = he_23->origin;
        Vertex *v3 = he_31->origin;

        if (DEBUG) std::cout << "he's: " << he_12->ToString() << ", " << he_23->ToString() << ", " << he_31->ToString() << std::endl;
        if (DEBUG) std::cout << "v's: " << v1 << ", " << v2 << ", " << v3 << std::endl;


        Vertex *v_opposite = nullptr;
        Vertex *v_colinear1  = nullptr, *v_colinear2  = nullptr;
        HalfEdge *he_col1_opp = nullptr, *he_opp_col2 = nullptr;
        HalfEdge *he_colinear  = nullptr;
        if (DCEL::Orientation2D(he_12->GetOriginCoord(), splitPoint, he_12->GetDestinyCoord()) == 0)
        {
            v_opposite = v3;
            v_colinear1 = v2;
            v_colinear2 = v1;
            he_colinear = he_12;
            he_col1_opp = he_23;
            he_opp_col2 = he_31;
        }
        else if (DCEL::Orientation2D(he_23->GetOriginCoord(), splitPoint, he_23->GetDestinyCoord()) == 0)
        {
            v_opposite = v1;
            v_colinear1  = v3;
            v_colinear2  = v2;
            he_colinear  = he_23;
            he_col1_opp = he_31;
            he_opp_col2 = he_12;
        }
        else if (DCEL::Orientation2D(he_31->GetOriginCoord(), splitPoint, he_31->GetDestinyCoord()) == 0)
        {
            v_opposite = v2;
            v_colinear1  = v1;
            v_colinear2  = v3;
            he_colinear  = he_31;
            he_col1_opp = he_12;
            he_opp_col2 = he_23;
        }
        else
        {
            if (DEBUG) std::cout << "Trying to split a triangle through a non-aligned point." << std::endl;
            return;
        }

        if (DEBUG) std::cout << "Special case!!!" << std::endl;
        if (DEBUG) std::cout << "he_colinear = " << he_colinear->ToString() << std::endl;
        if (DEBUG) std::cout << "he_colinear_face = " << he_colinear->face->ToString() << std::endl;
        if (DEBUG) std::cout << "he_colinear_twin = " << (!he_colinear->twin ? "----" : he_colinear->twin->ToString()) << std::endl;
        if (DEBUG) std::cout << "he_colinear_twin_face = " << (!he_colinear->twin ? "----" : he_colinear->twin->face->ToString()) << std::endl;
        if (DEBUG) std::cout << "v_opposite = " << v_opposite->coord << std::endl;

        HalfEdge *he_v_col1 = new HalfEdge(), *he_col2_v = new HalfEdge(),
                 *he_v_opp  = new HalfEdge(), *he_opp_v  = new HalfEdge();
        Face *f_v_col1_opp = new Face(),
             *f_v_opp_col2 = new Face();

        // Halfedge's nexts
        he_v_col1->next = he_col1_opp; he_col1_opp->next = he_opp_v; he_opp_v->next = he_v_col1;
        he_col2_v->next = he_v_opp; he_v_opp->next = he_opp_col2; he_opp_col2->next = he_col2_v;

        // Halfedges origins
        he_v_col1->origin = he_v_opp->origin = newVertex;
        he_col2_v->origin = v_colinear2;
        he_opp_v->origin = v_opposite;

        // Halfedge's twins
        he_v_col1->twin = he_col2_v->twin = he_colinear->twin;
        he_v_opp->twin = he_opp_v; he_opp_v->twin = he_v_opp;
        he_colinear->twin->twin = he_v_col1; // he_v_col2 would be good too

        // Halfedge's faces
        he_v_col1->face = he_col1_opp->face = he_opp_v->face = f_v_col1_opp;
        he_col2_v->face = he_opp_col2->face = he_v_opp->face = f_v_opp_col2;

        // Face boundaries
        f_v_col1_opp->boundary = he_v_col1;
        f_v_opp_col2->boundary = he_col2_v;

        // Push back everything
        vertices.push_back(newVertex);
        halfEdges.push_back(he_v_col1); halfEdges.push_back(he_v_opp);
        halfEdges.push_back(he_col2_v); halfEdges.push_back(he_opp_v);
        faces.push_back(f_v_col1_opp); faces.push_back(f_v_opp_col2); // Two faces :)
        if (DEBUG) { std::cout << "Adding f_v_col1_opp: "; f_v_col1_opp->Print(); }
        if (DEBUG) { std::cout << "Adding f_v_opp_col2: "; f_v_opp_col2->Print(); }

        *he_v_col1_ret = he_v_col1;
        *he_col2_v_ret = he_col2_v;
        *newTriangle1 = f_v_col1_opp;
        *newTriangle2 = f_v_opp_col2;

        tri->valid = false;
    }

    void SplitTriangleInTwo(Face *tri, const Vector3 &splitPoint, Vertex **newVertex,
                            HalfEdge **he_v_col1_ret, HalfEdge **he_col2_v_ret,
                            Face **newTriangle1, Face **newTriangle2)
    {
        *newVertex = new Vertex(splitPoint);
        SplitTriangleInTwo(tri, splitPoint, *newVertex, he_v_col1_ret, he_col2_v_ret, newTriangle1, newTriangle2);
    }

    // Must do this for every triangle adjacent to newVertex,
    // so tri is just one of those adjacent triangles to newVertex that must be fixed
    virtual bool FixDelaunay(Face *incidentTriToNewVertex, Vertex *newVertex,
                             Face **newFlippedFace1, Face **newFlippedFace2, bool *wasConcyclic)
    {
        // incidentTriToNewVertex is incident to newVertex
        // Have to check whether newVertex is inside the circumcircle
        // in the opposite face to newVertex & incidentTriToNewVertex. If it is inside, then flip the
        // edge opposite to incidentTriToNewVertex.

        *wasConcyclic = false;
        if (DEBUG) { std::cout << "fixing delaunay in: "; incidentTriToNewVertex->Print(); }
        for (int i = 0; i < 3; ++i)
        {
            HalfEdge *he = incidentTriToNewVertex->GetHalfEdge(i);
            if (he->twin == nullptr) { if (DEBUG) std::cout << "LIMIT FACE, skipping fix" << std::endl; return false; }
        }

        HalfEdge *he_oppositeToNewVertex = nullptr;
        for (int i = 0; i < 3; ++i)
        {
            HalfEdge *he = incidentTriToNewVertex->GetHalfEdge(i);
            if (DEBUG) std::cout << "Checking if (" << he->GetOriginCoord() << ", " << he->GetDestinyCoord() << ") is opposed" << std::endl;
           // if (DEBUG) std::cout << "Looking for oppToNewVertex: " << he->GetOriginCoord() << ", " << he->GetDestinyCoord() << std::endl;
            //if (he && he->origin != newVertex && he->twin->origin != newVertex)
            if (!Intersects(LineSegment(newVertex->coord, newVertex->coord), he->GetLineSegment()))
            {
           //     if (DEBUG) std::cout << "Found :)" << std::endl;
                he_oppositeToNewVertex = he;
                if (DEBUG) std::cout << "Found he_oppToNewVertex: (" << he->GetOriginCoord() << ", " << he->GetDestinyCoord() << ")" << std::endl;
                // break;
                if (he_oppositeToNewVertex->twin == nullptr) { if (DEBUG) std::cout << "SUPERTRI FACE, skipping fix" << std::endl; continue; }

                Face *oppositeFaceToNewVertex = he_oppositeToNewVertex->face == incidentTriToNewVertex ?
                            (he_oppositeToNewVertex->twin->face) : he_oppositeToNewVertex->face;
                if (oppositeFaceToNewVertex)
                {
                    if (DEBUG) std::cout << "oppositeFaceToNewVertex = (" << oppositeFaceToNewVertex->GetTriangle()[0] << ", " << oppositeFaceToNewVertex->GetTriangle()[1] << ", " << oppositeFaceToNewVertex->GetTriangle()[2] << ")" << std::endl;
                    if (PointInsideTriangleCircle(newVertex->coord, oppositeFaceToNewVertex->GetTriangle()))
                    {
                        if (DEBUG) std::cout << "Have to flip it" << std::endl;
                        Triangle oppTri = oppositeFaceToNewVertex->GetTriangle();
                        bool isConcyclic = AreConcyclic(newVertex->coord, oppTri[0], oppTri[1], oppTri[2]);
                        if (DEBUG && isConcyclic) { std::cout << "CONCYCLIC:" << newVertex->coord << ", " << oppTri[0] << ", " << oppTri[1] << ", " << oppTri[2] << std::endl; }

                        if (isConcyclic)
                        {
                            *wasConcyclic = true;
                            continue;
                        }
                        else
                        {
                            FlipEdge(he_oppositeToNewVertex, newFlippedFace1, newFlippedFace2);
                            return true;
                        }

                        return true;
                        if (DEBUG) std::cout << "Flipped " << oppositeFaceToNewVertex  << std::endl;
                    }
                    else
                    {
                        if (DEBUG) std::cout << "DONT Have to flip it" << std::endl;
                    }
                }
            }
        }
        return false;
    }

    // Creates a vertex inside an existing tri, producing 3 new adjacent triangles inside
    // the existing one.
    virtual void AddVertexInsideTri(Face *tri, const Vector3 &new_coord) // outputs
    {
        std::list<Face*> incidentTrianglesToP; // For later to fix Delaunay

        Vertex *newVertex = nullptr;
        bool inBoundary;
        HalfEdge *inBoundaryHalfEdge = nullptr;
        tri->Contains(new_coord, &inBoundary, &inBoundaryHalfEdge);
        if (inBoundary)
        {
            // Treating special case of vertex in the middle of an edge.
            // We must split the two triangles adjacent of this edge into two
            // So, from 2 triangles we will get 4 (an split for each (2+2))

            // Must be retrieve before splitting!
            Face *splitTri1 = inBoundaryHalfEdge->face;
            Face *splitTri2 = inBoundaryHalfEdge->twin ? inBoundaryHalfEdge->twin->face : nullptr;

            Face *newTriangle1, *newTriangle2;
            HalfEdge *splitTri1_he_col1_to_v, *splitTri1_he_v_to_col2;
            SplitTriangleInTwo(splitTri1, new_coord, &newVertex,
                               &splitTri1_he_col1_to_v, &splitTri1_he_v_to_col2,
                               &newTriangle1, &newTriangle2);
            incidentTrianglesToP.push_back(newTriangle1);
            incidentTrianglesToP.push_back(newTriangle2);

            if (splitTri2)
            {
                Face *newTriangle3, *newTriangle4;
                HalfEdge *splitTri2_he_col1_to_v, *splitTri2_he_v_to_col2;
                SplitTriangleInTwo(splitTri2, new_coord, newVertex, // Reuse created newVertex
                                   &splitTri2_he_col1_to_v, &splitTri2_he_v_to_col2,
                                   &newTriangle3, &newTriangle4);
                incidentTrianglesToP.push_back(newTriangle3);
                incidentTrianglesToP.push_back(newTriangle4);

                splitTri2_he_col1_to_v->twin = splitTri1_he_v_to_col2;
                splitTri1_he_v_to_col2->twin = splitTri2_he_col1_to_v;
                splitTri1_he_col1_to_v->twin = splitTri2_he_v_to_col2;
                splitTri2_he_v_to_col2->twin = splitTri1_he_col1_to_v;
            }
        }
        else
        {
            // Normal case:
            //   - Split the face(tri) in other 3 new triangles

            // Retrieve existing elements of tri
            HalfEdge *he_12 = tri->boundary;
            HalfEdge *he_23 = tri->boundary->next;
            HalfEdge *he_31 = tri->boundary->next->next;
            Vertex *v1 = he_12->GetOrigin();
            Vertex *v2 = he_23->GetOrigin();
            Vertex *v3 = he_31->GetOrigin();

            // New elements of tri
            newVertex = new Vertex(new_coord);
            HalfEdge *he_v_1 = new HalfEdge(), *he_v_2 = new HalfEdge(), *he_v_3 = new HalfEdge();
            HalfEdge *he_1_v = new HalfEdge(), *he_2_v = new HalfEdge(), *he_3_v = new HalfEdge();
            Face *f_v12 = new Face(), *f_v23 = new Face(), *f_v31 = new Face();

            // Change stuff

            // Link halfEdges to faces
            he_v_1->face = he_12->face = he_2_v->face = f_v12; // Face v12
            he_v_2->face = he_23->face = he_3_v->face = f_v23; // Face v23
            he_1_v->face = he_v_3->face = he_31->face = f_v31; // Face v31

            // HalfEdges twins
            he_v_1->twin = he_1_v; he_1_v->twin = he_v_1;
            he_v_2->twin = he_2_v; he_2_v->twin = he_v_2;
            he_v_3->twin = he_3_v; he_3_v->twin = he_v_3;

            // HalfEdges next's
            he_v_1->next = he_12; he_12->next = he_2_v; he_2_v->next = he_v_1;
            he_v_2->next = he_23; he_23->next = he_3_v; he_3_v->next = he_v_2;
            he_v_3->next = he_31; he_31->next = he_1_v; he_1_v->next = he_v_3;

            //tri->boundary = he_12;

            // Face boundaries
            f_v12->boundary = he_v_1;
            f_v23->boundary = he_v_2;
            f_v31->boundary = he_v_3;

            // HalfEdges origins
            he_v_1->origin = he_v_2->origin = he_v_3->origin = newVertex;
            he_1_v->origin = v1;
            he_2_v->origin = v2;
            he_3_v->origin = v3;

            // Push back everything
            vertices.push_back(newVertex);
            halfEdges.push_back(he_v_1); halfEdges.push_back(he_v_2); halfEdges.push_back(he_v_3);
            halfEdges.push_back(he_1_v); halfEdges.push_back(he_2_v); halfEdges.push_back(he_3_v);
            faces.push_back(f_v12); faces.push_back(f_v23); faces.push_back(f_v31);

            incidentTrianglesToP.push_back(f_v12);
            incidentTrianglesToP.push_back(f_v23);
            incidentTrianglesToP.push_back(f_v31);

            if (DEBUG) std::cout << "Added vertex " << newVertex->coord << " with edges " <<
                                    he_v_1->ToString() << ", " << he_v_2->ToString() << ", " << he_v_3->ToString() << std::endl;
        }

        // Remove old stuff
        tri->valid = false;
        // VERY VERY SLOW!!!
        // faces.remove(tri); // Remove face
        //delete tri;

        if (tri != faces.front())
        {
            bool allTrianglesIncidentToPCorrect = false;
            while (!allTrianglesIncidentToPCorrect)
            {
                if (DEBUG) { std::cout << "incidentTrianglesToP: ("; for (Face *incidentTriToP : incidentTrianglesToP) std::cout << incidentTriToP->ToString() << ", "; std::cout << ")" << std::endl; }
                allTrianglesIncidentToPCorrect = true;
                for (Face *incidentTriToP : incidentTrianglesToP)
                {
                    if (DEBUG) std::cout << "--" << std::endl;
                    if (DEBUG) std::cout << "FixDelaunay on (" << incidentTriToP->GetTriangle()[0] << ", " << incidentTriToP->GetTriangle()[1] << ", " << incidentTriToP->GetTriangle()[2] << ")" << std::endl;
                    Face *newFlippedFace1 = nullptr, *newFlippedFace2 = nullptr;
                    bool wasConcyclic = false;
                    if (FixDelaunay(incidentTriToP, newVertex, &newFlippedFace1, &newFlippedFace2, &wasConcyclic) &&
                        newFlippedFace1 != nullptr && newFlippedFace2 != nullptr)
                    {
                        allTrianglesIncidentToPCorrect = false;
                        incidentTrianglesToP.remove(incidentTriToP);
                        if (!wasConcyclic)
                        {
                            incidentTrianglesToP.push_back(newFlippedFace1);
                            incidentTrianglesToP.push_back(newFlippedFace2);
                        }
                        break;
                    }
                }
            }
        }

        RegisterVertexInVertexGrid(newVertex);
    }

    virtual void AddFaceTri(const Vector3 &c1, const Vector3 &c2, const Vector3 &c3)
    {
        Vertex *v1 = new Vertex(c1), *v2 = new Vertex(c2), *v3 = new Vertex(c3);
        HalfEdge *he_12 = new HalfEdge(), *he_23 = new HalfEdge(), *he_31 = new HalfEdge();
        //HalfEdge *he_21 = new HalfEdge(), *he_32 = new HalfEdge(), *he_13 = new HalfEdge();
        Face *f = new Face();

        //he_21->origin = v2; he_32->origin = v3; he_13->origin = v1;
        he_12->origin = v1; he_23->origin = v2; he_31->origin = v3;

        //he_12->face = he_23->face = he_31->face = he_21->face = he_32->face = he_13->face = f;
        he_12->face = he_23->face = he_31->face = f;

        he_12->next = he_23; he_23->next = he_31; he_31->next = he_12;
        //he_21->next = he_13; he_13->next = he_32; he_32->next = he_21;

        he_12->twin = nullptr; //he_21->twin = he_12;
        he_23->twin = nullptr; //he_32->twin = he_23;
        he_31->twin = nullptr; //he_13->twin = nullptr;//

        f->boundary = he_12;

        vertices.push_back(v1);     vertices.push_back(v2);     vertices.push_back(v3);
        halfEdges.push_back(he_12); halfEdges.push_back(he_23); halfEdges.push_back(he_31);
       // halfEdges.push_back(he_21); halfEdges.push_back(he_32); halfEdges.push_back(he_13);
        faces.push_back(f);
        RegisterVertexInVertexGrid(v1);
        RegisterVertexInVertexGrid(v2);
        RegisterVertexInVertexGrid(v3);
    }

    void RegisterVertexInVertexGrid(DCEL::Vertex *v)
    {
        Vector2 p = v->coord;
        vertexGrid[int((p.x()-minX)/xSize * GridSize)][int((p.y()-minY)/ySize * GridSize)][0] = v;
    }

    Vertex* GetCloseVertexTo(const Vector2 &p)
    {
        return vertexGrid[int((p.x()-minX)/xSize * GridSize)][int((p.y()-minY)/ySize * GridSize)][0];
    }

    std::list<Vertex*> vertices;
    std::list<HalfEdge*> halfEdges;
    std::vector<Face*> faces;

    typedef std::vector<DCEL::Vertex*> VertexVector;
    typedef std::vector<VertexVector> Tile;
    const int GridSize = 100;
    float xSize = 0.0f, ySize = 0.0f, minX = 0.0f, minY = 0.0f;
    std::vector<Tile> vertexGrid;
};



/** \ingroup Geometry */
//! A triangulation of a set of points in the plane.
class Triangulation : public TriangulationBase
{
public:

    Triangulation();
    ~Triangulation();

    bool OutsideBoundaries(const std::vector< std::vector<int> > &boundariesIndices, const std::vector<Vector3> &points,
                          const Vector3 &point) const;
    bool OutsideBoundaries(const std::vector< std::vector<int> > &boundariesIndices, const std::vector<Vector3> &points,
                          const Triangle &tri) const;
    double Dot(const Vector2 &v1, const Vector2 &v2) const;

    void triangulate(const std::vector<Vector3>& ps,
                     const std::vector<int>& idxs,
                     std::vector<LineSegmentEnt>& segments,
                     std::vector<TriangleEnt>& triangles,
                     std::vector<TriangleEnt>& triangles_pruned);
};

} // namespace geoc


#endif //_GEOC_TRIANGULATION_H
