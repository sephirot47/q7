#include <geoc/geometry/LineSegment.h>
#include <geoc/math/Math.h>


using namespace geoc;
using namespace std;

// Given (p,q,r) colinear points => returns if r is between(p,q).
inline bool between(const Vector3 &p, const Vector3 &q, const Vector3 &r)
{
    return r[X] >= min(p[X], q[X]) && r[X] <= max(p[X], q[X]) &&
           r[Y] >= min(p[Y], q[Y]) && r[Y] <= max(p[Y], q[Y]);
}

void geoc::classifyIntersection(const LineSegment& s, const LineSegment& t,
                                Colour3& colour, std::string& desc)
{
    //Exercise 1.
    Vector3 p0 = s.point(0), p1 = s.point(1);
    Vector3 p2 = t.point(0), p3 = t.point(1);

    // Calculate orientations
    float o012 = Math::orientation2D(p0, p1, p2);
    float o013 = Math::orientation2D(p0, p1, p3);
    float o230 = Math::orientation2D(p2, p3, p0);
    float o231 = Math::orientation2D(p2, p3, p1);

    // s and t collide
    if (o012 != o013 && o230 != o231)
    {
        // One of the points is over one segment
        if (o012 * o013 * o230 * o231 == 0) // One of them is colinear
        {
            colour = Colour3(0, 0, 1); desc = "Partial collision";
        }
        else
        {
            colour = Colour3(0, 1, 0); desc = "Total collision";
        }
    }
    else if (o012 == 0 && o013 == 0 && o230 == 0 && o231 == 0)
    {   // One point of either s or t is over the other segment
        if (between(p0, p1, p2) || between(p0, p1, p3) ||
            between(p2, p3, p0) || between(p2, p3, p1))
        {   // Colliding
            colour = Colour3(1, 1, 0); desc = "They are colinear and colliding";
        }
        else
        {   // NOT Colliding
            colour = Colour3(1, 0, 0); desc = "They are colinear and NOT colliding";
        }
    }
    else
    {
        colour = Colour3(1, 1, 1); desc = "Not colliding.";
    }
}
