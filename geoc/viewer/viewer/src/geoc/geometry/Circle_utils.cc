#include <geoc/geometry/Circle.h>
#include <geoc/scene/Point.h>
#include <geoc/math/Math.h>

using namespace geoc;
using namespace std;

void geoc::classify(const Circle& c, const Vector3& p, Colour3& colour, std::string& desc)
{
    //Exercise 3.
    colour = Colour3(1, 0, 0); desc = "Outside";

    // If it's ccw, inverse orientation
    int cwMultiplier = Math::orientation2D(c[0], c[1], c[2]) > 0 ? 1 : -1;
    num orientation = Math::orientation25D(Vector2(c[0][X], c[0][Y]),
                                           Vector2(c[1][X], c[1][Y]),
                                           Vector2(c[2][X], c[2][Y]),
                                           Vector2(p[X], p[Y])) * cwMultiplier;
    if (orientation == 0)
    {
        colour = Colour3(1, 1, 0); desc = "Border";
    }
    else if (orientation > 0)
    {
        colour = Colour3(0, 1, 0); desc = "Inside";
    }
}

