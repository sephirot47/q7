#include <geoc/geometry/Triangle.h>
#include <geoc/geometry/LineSegment.h>
#include <geoc/scene/Point.h>
#include <geoc/math/Math.h>
#include <geoc/GeocException.h>


using namespace geoc;
using namespace std;

// Given (p,q,r) colinear points => returns if r is between(p,q).
inline bool between(const Vector3 &p, const Vector3 &q, const Vector3 &r)
{
    return r[X] >= min(p[X], q[X]) && r[X] <= max(p[X], q[X]) &&
           r[Y] >= min(p[Y], q[Y]) && r[Y] <= max(p[Y], q[Y]);
}

void geoc::classify(const Triangle& tri, const Vector3& p,
                    Colour3& colour, std::string& desc)
{
    //Exercise 2.
    Triangle t = tri;
    bool ccw = Math::orientation2D(t[0], t[1], t[2]) < 0;
    if (!ccw) { t = Triangle(t[2], t[1], t[0]); } // Always in ccw

    float o01 = Math::orientation2D(t[0], t[1], p);
    float o12 = Math::orientation2D(t[1], t[2], p);
    float o20 = Math::orientation2D(t[2], t[0], p);
    if (p == t[0] || p == t[1] || p == t[2])
    {
        colour = Colour3(1, 0, 1);
        desc = "On vertex";
    }
    else if (o01 < 0 && o12 < 0 && o20 < 0)
    {
        colour = Colour3(0, 1, 0);
        desc = "Inside";
    }
    else if ((o01 == 0 && between(t[0], t[1], p)) ||
             (o12 == 0 && between(t[1], t[2], p)) ||
             (o20 == 0 && between(t[2], t[0], p)) )
    {
        colour = Colour3(1, 1, 0);
        desc = "On side";
    }
    else
    {
        colour = Colour3(1, 0, 0);
        desc = "Exterior";
    }
}
