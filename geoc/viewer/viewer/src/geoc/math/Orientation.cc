#include <geoc/math/Math.h>
#include <geoc/math/Vector.h>

using namespace geoc;

num Math::orientation2D(const Vector3& p, const Vector3& q, const Vector3& r)
{
    //Exercise 1.
    float crossProduct = ((q[X] - p[X]) * (r[Y] - p[Y]) - (q[Y] - p[Y]) * (r[X] - p[X]));
    return crossProduct == 0 ? 0 : ( crossProduct > 0 ? 1 : -1 ) ;
}

num Math::orientation25D(const Vector2& p, const Vector2& q, const Vector2& r, const Vector2& t)
{
    //Exercise 3.

    // Get the points projected to the paraboloid
    Vector3 p3 = Vector3(p[X], p[Y], p[X] * p[X] + p[Y] * p[Y]);
    Vector3 q3 = Vector3(q[X], q[Y], q[X] * q[X] + q[Y] * q[Y]);
    Vector3 r3 = Vector3(r[X], r[Y], r[X] * r[X] + r[Y] * r[Y]);
    Vector3 t3 = Vector3(t[X], t[Y], t[X] * t[X] + t[Y] * t[Y]);

    // Get the oriented volume of the tetrahedron
    num det = ( dot(p3-t3, cross(q3-t3, r3-t3)) ); // / 6.0f;
    return det;
}
