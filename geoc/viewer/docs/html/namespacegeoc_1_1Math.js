var namespacegeoc_1_1Math =
[
    [ "abs", "namespacegeoc_1_1Math.html#a193c9af104c4f71837bbe04f966e5b3e", null ],
    [ "calculateForwardVector", "namespacegeoc_1_1Math.html#ae434f4243a0009056a12ca7f9b21416b", null ],
    [ "calculateRightVector", "namespacegeoc_1_1Math.html#af9bd31566652a76b08b43def7f3dafeb", null ],
    [ "calculateUpVector", "namespacegeoc_1_1Math.html#a373ccaa5b0b0d086fb15d1be05ceb302", null ],
    [ "calculateVectors", "namespacegeoc_1_1Math.html#a3b47fb479b0ac6eedca50cdec5f8ec8c", null ],
    [ "calculateVectors", "namespacegeoc_1_1Math.html#aac4737d829943be7465e51f7c68e0a9f", null ],
    [ "createPitchFromForward", "namespacegeoc_1_1Math.html#af983172712c84c08fcbe1bd97d2cfdb9", null ],
    [ "createRollFromRight", "namespacegeoc_1_1Math.html#aab7cbb9a466a629619c82919a2245cff", null ],
    [ "createYawFromForward", "namespacegeoc_1_1Math.html#a2b4dccd92d21883a76d6a0c2b9de9667", null ],
    [ "createYawPitchRoll", "namespacegeoc_1_1Math.html#af0029a6572b3a075cb9c8abe436cfb07", null ],
    [ "makeCounterClockwise", "namespacegeoc_1_1Math.html#a412172ced2285754a4f495ed67adcfd3", null ],
    [ "max", "namespacegeoc_1_1Math.html#a34e43007139cf8068252713efb13db7c", null ],
    [ "nextMultiple", "namespacegeoc_1_1Math.html#a34b7b649412efc8c40c11eed52ebe2fa", null ],
    [ "nextPowerOfTwo", "namespacegeoc_1_1Math.html#ac01d9d7de18fa38da408bf87f91c9a6f", null ],
    [ "orientation2D", "namespacegeoc_1_1Math.html#a3d45386a47b5027805baf79e10164323", null ],
    [ "orientation3D", "namespacegeoc_1_1Math.html#a146f9e2ed8545273f5e9206108226187", null ],
    [ "round", "namespacegeoc_1_1Math.html#a8e319cba9ac2724d87acbedf46e2f196", null ],
    [ "signOf", "namespacegeoc_1_1Math.html#ae862580bfcaa2071e61d7b4052324720", null ],
    [ "square", "namespacegeoc_1_1Math.html#a001220b0e0dcca142d4000431fb10eb6", null ],
    [ "viewportToObject", "namespacegeoc_1_1Math.html#ac36eec228a6b37a9826c70070403a2e6", null ],
    [ "viewportToWorld", "namespacegeoc_1_1Math.html#adf54709804cc2ae02dc687a121172f43", null ]
];