var algorithms_8h =
[
    [ "foldl", "namespacegeoc.html#a9256ca660be7f0353c81235a8072f02d", null ],
    [ "foldl", "namespacegeoc.html#ae5f4d89c154369e6157ae005428b47c1", null ],
    [ "foldr", "namespacegeoc.html#adbf473529dbcb515f1eb8595a6072aa2", null ],
    [ "foldr", "namespacegeoc.html#a3b2dd2c6468111932a4d197e4de04bd4", null ],
    [ "map", "namespacegeoc.html#aa2dfcc3735cdde075f05a2b0af1d902b", null ],
    [ "map", "namespacegeoc.html#ad6de28a2639e85f9fe20e744b4e21063", null ],
    [ "map", "namespacegeoc.html#a8cf13b5269c67166a2890384024035d4", null ],
    [ "map", "namespacegeoc.html#a4d5c5ff5b493fed32c7aada97d2b422e", null ],
    [ "mapM", "namespacegeoc.html#a30fd1c55e69880e9e9ec8c0ce1812266", null ],
    [ "mapM", "namespacegeoc.html#a8bf6154437ce3655c15e7a0f5b93dfa3", null ],
    [ "zipWith", "namespacegeoc.html#a3b24a51447fba62c11a567b0e122d28f", null ],
    [ "zipWith", "namespacegeoc.html#a229ae4811abde6b9a395eac3be0c3b72", null ]
];