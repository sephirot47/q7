var NAVTREE =
[
  [ "Geoc Viewer", "index.html", [
    [ "Geoc Viewer Documentation", "index.html", null ],
    [ "Modules", "modules.html", [
      [ "Application", "group__App.html", null ],
      [ "CGAL", "group__CGAL.html", null ],
      [ "Camera", "group__Camera.html", null ],
      [ "Camera Control", "group__CamCtrl.html", null ],
      [ "Geometry", "group__Geometry.html", null ],
      [ "Graphics", "group__Gfx.html", null ],
      [ "Input and Output", "group__IO.html", [
        [ "Device Input", "group__Input.html", null ],
        [ "Object Loading", "group__Loader.html", null ],
        [ "Output Streaming", "group__Output.html", null ],
        [ "Screen Input", "group__Screen.html", null ]
      ] ],
      [ "Math", "group__Math.html", null ],
      [ "Scene Management", "group__Scene.html", null ]
    ] ],
    [ "Data Structures", "annotated.html", [
      [ "geoc::BoundingBox< N >", "classgeoc_1_1BoundingBox.html", null ],
      [ "geoc::CamCtrlContext", "classgeoc_1_1CamCtrlContext.html", null ],
      [ "geoc::CamCtrlState", "classgeoc_1_1CamCtrlState.html", null ],
      [ "geoc::CamCtrlState2D", "classgeoc_1_1CamCtrlState2D.html", null ],
      [ "geoc::CamCtrlState3D", "classgeoc_1_1CamCtrlState3D.html", null ],
      [ "geoc::Camera", "classgeoc_1_1Camera.html", null ],
      [ "geoc::CgalTriangulation", "classgeoc_1_1CgalTriangulation.html", null ],
      [ "geoc::Circle", "classgeoc_1_1Circle.html", null ],
      [ "geoc::Circle_3_cons", "classgeoc_1_1Circle__3__cons.html", null ],
      [ "geoc::CircleEnt", "classgeoc_1_1CircleEnt.html", null ],
      [ "geoc::CircleState", "classgeoc_1_1CircleState.html", null ],
      [ "geoc::Command", "classgeoc_1_1Command.html", null ],
      [ "geoc::ConsoleOutput", "classgeoc_1_1ConsoleOutput.html", null ],
      [ "geoc::Construct_Vector2_iterator", "classgeoc_1_1Construct__Vector2__iterator.html", null ],
      [ "geoc::Construct_Vector3_iterator", "classgeoc_1_1Construct__Vector3__iterator.html", null ],
      [ "geoc::Entity", "classgeoc_1_1Entity.html", null ],
      [ "geoc::FileOutput", "classgeoc_1_1FileOutput.html", null ],
      [ "geoc::Font", "classgeoc_1_1Font.html", null ],
      [ "geoc::GeocApplication", "classgeoc_1_1GeocApplication.html", null ],
      [ "geoc::GeocException", "classgeoc_1_1GeocException.html", null ],
      [ "geoc::GeocWidget", "classgeoc_1_1GeocWidget.html", null ],
      [ "geoc::GeocWidgetInterface", "classgeoc_1_1GeocWidgetInterface.html", null ],
      [ "geoc::Graphics", "classgeoc_1_1Graphics.html", null ],
      [ "geoc::IdleState", "classgeoc_1_1IdleState.html", null ],
      [ "geoc::ILoader", "classgeoc_1_1ILoader.html", null ],
      [ "geoc::Input", "classgeoc_1_1Input.html", null ],
      [ "geoc::IScreenState", "classgeoc_1_1IScreenState.html", null ],
      [ "geoc::Keyboard", "classgeoc_1_1Keyboard.html", null ],
      [ "geoc::LineSegment", "classgeoc_1_1LineSegment.html", null ],
      [ "geoc::LineSegmentEnt", "classgeoc_1_1LineSegmentEnt.html", null ],
      [ "geoc::LineSegmentState", "classgeoc_1_1LineSegmentState.html", null ],
      [ "geoc::Loader< T >", "classgeoc_1_1Loader.html", null ],
      [ "geoc::Mouse", "classgeoc_1_1Mouse.html", null ],
      [ "geoc::ObjectLoader", "classgeoc_1_1ObjectLoader.html", null ],
      [ "geoc::Observer< T >", "classgeoc_1_1Observer.html", null ],
      [ "geoc::OutputStream", "classgeoc_1_1OutputStream.html", null ],
      [ "geoc::OutputSystem", "classgeoc_1_1OutputSystem.html", null ],
      [ "geoc::Point", "classgeoc_1_1Point.html", null ],
      [ "geoc::PointState", "classgeoc_1_1PointState.html", null ],
      [ "geoc::Quaternion", "classgeoc_1_1Quaternion.html", null ],
      [ "geoc::SceneManager", "classgeoc_1_1SceneManager.html", null ],
      [ "geoc::ScreenInput", "classgeoc_1_1ScreenInput.html", null ],
      [ "geoc::ScreenState< T >", "classgeoc_1_1ScreenState.html", null ],
      [ "geoc::ScrollList", "classgeoc_1_1ScrollList.html", null ],
      [ "geoc::Segment_3_cons", "classgeoc_1_1Segment__3__cons.html", null ],
      [ "geoc::Spatial", "classgeoc_1_1Spatial.html", null ],
      [ "geoc::Subject< T >", "classgeoc_1_1Subject.html", null ],
      [ "geoc::Timer", "classgeoc_1_1Timer.html", null ],
      [ "geoc::Triangle", "classgeoc_1_1Triangle.html", null ],
      [ "geoc::Triangle_3_cons", "classgeoc_1_1Triangle__3__cons.html", null ],
      [ "geoc::TriangleEnt", "classgeoc_1_1TriangleEnt.html", null ],
      [ "geoc::TriangleState", "classgeoc_1_1TriangleState.html", null ],
      [ "geoc::Triangulation", "classgeoc_1_1Triangulation.html", null ],
      [ "geoc::TriangulationBase", "classgeoc_1_1TriangulationBase.html", null ],
      [ "geoc::TriangulationEnt", "classgeoc_1_1TriangulationEnt.html", null ],
      [ "geoc::Vector< T, N >", "classgeoc_1_1Vector.html", null ],
      [ "geoc::Viewer_Kernel< FT >", "structgeoc_1_1Viewer__Kernel.html", null ],
      [ "geoc::Viewer_Point_2_cons< K, OldK >", "classgeoc_1_1Viewer__Point__2__cons.html", null ],
      [ "geoc::Viewer_Point_3_cons< K, OldK >", "classgeoc_1_1Viewer__Point__3__cons.html", null ],
      [ "geoc::ViewerCartesian_base< K_, K_Base >", "classgeoc_1_1ViewerCartesian__base.html", null ]
    ] ],
    [ "Data Structure Index", "classes.html", null ],
    [ "Class Hierarchy", "hierarchy.html", [
      [ "geoc::BoundingBox< N >", "classgeoc_1_1BoundingBox.html", null ],
      [ "geoc::CamCtrlContext", "classgeoc_1_1CamCtrlContext.html", null ],
      [ "geoc::CamCtrlState", "classgeoc_1_1CamCtrlState.html", [
        [ "geoc::CamCtrlState2D", "classgeoc_1_1CamCtrlState2D.html", [
          [ "geoc::CamCtrlState3D", "classgeoc_1_1CamCtrlState3D.html", null ]
        ] ]
      ] ],
      [ "geoc::Circle", "classgeoc_1_1Circle.html", [
        [ "geoc::CircleEnt", "classgeoc_1_1CircleEnt.html", null ]
      ] ],
      [ "geoc::Circle_3_cons", "classgeoc_1_1Circle__3__cons.html", null ],
      [ "geoc::Command", "classgeoc_1_1Command.html", null ],
      [ "geoc::Construct_Vector2_iterator", "classgeoc_1_1Construct__Vector2__iterator.html", null ],
      [ "geoc::Construct_Vector3_iterator", "classgeoc_1_1Construct__Vector3__iterator.html", null ],
      [ "geoc::Entity", "classgeoc_1_1Entity.html", [
        [ "geoc::CircleEnt", "classgeoc_1_1CircleEnt.html", null ],
        [ "geoc::LineSegmentEnt", "classgeoc_1_1LineSegmentEnt.html", null ],
        [ "geoc::Point", "classgeoc_1_1Point.html", null ],
        [ "geoc::TriangleEnt", "classgeoc_1_1TriangleEnt.html", null ],
        [ "geoc::TriangulationEnt", "classgeoc_1_1TriangulationEnt.html", null ]
      ] ],
      [ "geoc::Font", "classgeoc_1_1Font.html", null ],
      [ "geoc::GeocApplication", "classgeoc_1_1GeocApplication.html", null ],
      [ "geoc::GeocException", "classgeoc_1_1GeocException.html", null ],
      [ "geoc::GeocWidget", "classgeoc_1_1GeocWidget.html", null ],
      [ "geoc::GeocWidgetInterface", "classgeoc_1_1GeocWidgetInterface.html", null ],
      [ "geoc::Graphics", "classgeoc_1_1Graphics.html", null ],
      [ "geoc::ILoader", "classgeoc_1_1ILoader.html", [
        [ "geoc::Loader< T >", "classgeoc_1_1Loader.html", null ]
      ] ],
      [ "geoc::Input", "classgeoc_1_1Input.html", null ],
      [ "geoc::IScreenState", "classgeoc_1_1IScreenState.html", [
        [ "geoc::IdleState", "classgeoc_1_1IdleState.html", null ],
        [ "geoc::ScreenState< T >", "classgeoc_1_1ScreenState.html", null ],
        [ "geoc::ScreenState< CircleEnt >", "classgeoc_1_1ScreenState.html", [
          [ "geoc::CircleState", "classgeoc_1_1CircleState.html", null ]
        ] ],
        [ "geoc::ScreenState< LineSegmentEnt >", "classgeoc_1_1ScreenState.html", [
          [ "geoc::LineSegmentState", "classgeoc_1_1LineSegmentState.html", null ]
        ] ],
        [ "geoc::ScreenState< Point >", "classgeoc_1_1ScreenState.html", [
          [ "geoc::PointState", "classgeoc_1_1PointState.html", null ]
        ] ],
        [ "geoc::ScreenState< TriangleEnt >", "classgeoc_1_1ScreenState.html", [
          [ "geoc::TriangleState", "classgeoc_1_1TriangleState.html", null ]
        ] ]
      ] ],
      [ "geoc::Keyboard", "classgeoc_1_1Keyboard.html", null ],
      [ "geoc::LineSegment", "classgeoc_1_1LineSegment.html", [
        [ "geoc::LineSegmentEnt", "classgeoc_1_1LineSegmentEnt.html", null ]
      ] ],
      [ "geoc::Mouse", "classgeoc_1_1Mouse.html", null ],
      [ "geoc::ObjectLoader", "classgeoc_1_1ObjectLoader.html", null ],
      [ "geoc::Observer< T >", "classgeoc_1_1Observer.html", null ],
      [ "geoc::OutputStream", "classgeoc_1_1OutputStream.html", [
        [ "geoc::ConsoleOutput", "classgeoc_1_1ConsoleOutput.html", null ],
        [ "geoc::FileOutput", "classgeoc_1_1FileOutput.html", null ],
        [ "geoc::OutputSystem", "classgeoc_1_1OutputSystem.html", null ],
        [ "geoc::ScrollList", "classgeoc_1_1ScrollList.html", null ]
      ] ],
      [ "geoc::Quaternion", "classgeoc_1_1Quaternion.html", null ],
      [ "geoc::ScreenInput", "classgeoc_1_1ScreenInput.html", null ],
      [ "geoc::Segment_3_cons", "classgeoc_1_1Segment__3__cons.html", null ],
      [ "geoc::Spatial", "classgeoc_1_1Spatial.html", [
        [ "geoc::Camera", "classgeoc_1_1Camera.html", null ],
        [ "geoc::SceneManager", "classgeoc_1_1SceneManager.html", null ]
      ] ],
      [ "geoc::Subject< T >", "classgeoc_1_1Subject.html", [
        [ "geoc::Loader< T >", "classgeoc_1_1Loader.html", null ],
        [ "geoc::ScreenState< T >", "classgeoc_1_1ScreenState.html", null ]
      ] ],
      [ "geoc::Timer", "classgeoc_1_1Timer.html", null ],
      [ "geoc::Triangle", "classgeoc_1_1Triangle.html", [
        [ "geoc::TriangleEnt", "classgeoc_1_1TriangleEnt.html", null ]
      ] ],
      [ "geoc::Triangle_3_cons", "classgeoc_1_1Triangle__3__cons.html", null ],
      [ "geoc::TriangulationBase", "classgeoc_1_1TriangulationBase.html", [
        [ "geoc::CgalTriangulation", "classgeoc_1_1CgalTriangulation.html", null ],
        [ "geoc::Triangulation", "classgeoc_1_1Triangulation.html", null ]
      ] ],
      [ "geoc::Vector< T, N >", "classgeoc_1_1Vector.html", null ],
      [ "geoc::Viewer_Kernel< FT >", "structgeoc_1_1Viewer__Kernel.html", null ],
      [ "geoc::Viewer_Point_2_cons< K, OldK >", "classgeoc_1_1Viewer__Point__2__cons.html", null ],
      [ "geoc::Viewer_Point_3_cons< K, OldK >", "classgeoc_1_1Viewer__Point__3__cons.html", null ],
      [ "geoc::ViewerCartesian_base< K_, K_Base >", "classgeoc_1_1ViewerCartesian__base.html", null ],
      [ "geoc::Subject< CircleEnt >", "classgeoc_1_1Subject.html", [
        [ "geoc::ScreenState< CircleEnt >", "classgeoc_1_1ScreenState.html", null ]
      ] ],
      [ "geoc::Subject< Entity >", "classgeoc_1_1Subject.html", [
        [ "geoc::SceneManager", "classgeoc_1_1SceneManager.html", null ]
      ] ],
      [ "geoc::Subject< LineSegmentEnt >", "classgeoc_1_1Subject.html", [
        [ "geoc::ScreenState< LineSegmentEnt >", "classgeoc_1_1ScreenState.html", null ]
      ] ],
      [ "geoc::Subject< Point >", "classgeoc_1_1Subject.html", [
        [ "geoc::ScreenState< Point >", "classgeoc_1_1ScreenState.html", null ]
      ] ],
      [ "geoc::Subject< TriangleEnt >", "classgeoc_1_1Subject.html", [
        [ "geoc::ScreenState< TriangleEnt >", "classgeoc_1_1ScreenState.html", null ]
      ] ]
    ] ],
    [ "Data Fields", "functions.html", null ],
    [ "Namespace List", "namespaces.html", [
      [ "CGAL", "namespaceCGAL.html", null ],
      [ "geoc", "namespacegeoc.html", null ],
      [ "geoc::Math", "namespacegeoc_1_1Math.html", null ]
    ] ],
    [ "Namespace Members", "namespacemembers.html", null ],
    [ "File List", "files.html", [
      [ "algorithms.h", "algorithms_8h.html", null ],
      [ "alphabet.h", "alphabet_8h.html", null ],
      [ "aux_functions.h", "aux__functions_8h.html", null ],
      [ "BoundingBox.h", "BoundingBox_8h.html", null ],
      [ "CamCtrlContext.h", "CamCtrlContext_8h.html", null ],
      [ "CamCtrlState.h", "CamCtrlState_8h.html", null ],
      [ "CamCtrlState2D.h", "CamCtrlState2D_8h.html", null ],
      [ "CamCtrlState3D.h", "CamCtrlState3D_8h.html", null ],
      [ "Camera.h", "Camera_8h.html", null ],
      [ "CgalTriangulation.h", "CgalTriangulation_8h.html", null ],
      [ "Circle.h", "Circle_8h.html", null ],
      [ "CircleEnt.h", "CircleEnt_8h.html", null ],
      [ "CircleState.h", "CircleState_8h.html", null ],
      [ "Command.h", "Command_8h.html", null ],
      [ "ConsoleOutput.h", "ConsoleOutput_8h.html", null ],
      [ "Constructors.hpp", "Constructors_8hpp.html", null ],
      [ "designer.h", "designer_8h.html", null ],
      [ "doxy.h", "doxy_8h.html", null ],
      [ "Entity.h", "Entity_8h.html", null ],
      [ "File_utils.h", "File__utils_8h.html", null ],
      [ "FileOutput.h", "FileOutput_8h.html", null ],
      [ "Font.h", "Font_8h.html", null ],
      [ "geoc.h", "geoc_8h.html", null ],
      [ "GeocApplication.h", "GeocApplication_8h.html", null ],
      [ "GeocException.h", "GeocException_8h.html", null ],
      [ "GeocWidget.h", "GeocWidget_8h.html", null ],
      [ "gl.h", "gl_8h.html", null ],
      [ "glu.h", "glu_8h.html", null ],
      [ "Graphics.h", "Graphics_8h.html", null ],
      [ "higher_order_cpp.h", "higher__order__cpp_8h.html", null ],
      [ "IdleState.h", "IdleState_8h.html", null ],
      [ "ILoader.h", "ILoader_8h.html", null ],
      [ "Input.h", "Input_8h.html", null ],
      [ "input_translation.h", "input__translation_8h.html", null ],
      [ "IScreenState.h", "IScreenState_8h.html", null ],
      [ "Kernel.h", "Kernel_8h.html", null ],
      [ "Keyboard.h", "Keyboard_8h.html", null ],
      [ "LineSegment.h", "LineSegment_8h.html", null ],
      [ "LineSegmentEnt.h", "LineSegmentEnt_8h.html", null ],
      [ "LineSegmentState.h", "LineSegmentState_8h.html", null ],
      [ "Loader.h", "Loader_8h.html", null ],
      [ "Math.h", "Math_8h.html", null ],
      [ "Mouse.h", "Mouse_8h.html", null ],
      [ "ObjectLoader.h", "ObjectLoader_8h.html", null ],
      [ "Observer.h", "Observer_8h.html", null ],
      [ "OutputStream.h", "OutputStream_8h.html", null ],
      [ "OutputSystem.h", "OutputSystem_8h.html", null ],
      [ "Point.h", "Point_8h.html", null ],
      [ "PointState.h", "PointState_8h.html", null ],
      [ "Quaternion.h", "Quaternion_8h.html", null ],
      [ "SceneManager.h", "SceneManager_8h.html", null ],
      [ "ScreenInput.h", "ScreenInput_8h.html", null ],
      [ "ScreenState.h", "ScreenState_8h.html", null ],
      [ "ScrollList.h", "ScrollList_8h.html", null ],
      [ "Spatial.h", "Spatial_8h.html", null ],
      [ "Subject.h", "Subject_8h.html", null ],
      [ "Timer.h", "Timer_8h.html", null ],
      [ "Triangle.h", "Triangle_8h.html", null ],
      [ "TriangleEnt.h", "TriangleEnt_8h.html", null ],
      [ "TriangleState.h", "TriangleState_8h.html", null ],
      [ "Triangulation.h", "Triangulation_8h.html", null ],
      [ "TriangulationBase.h", "TriangulationBase_8h.html", null ],
      [ "TriangulationEnt.h", "TriangulationEnt_8h.html", null ],
      [ "update_requests.h", "update__requests_8h.html", null ],
      [ "Vector.h", "Vector_8h.html", null ],
      [ "Vector_fwd_decl.h", "Vector__fwd__decl_8h.html", null ],
      [ "Vector_iterators.h", "Vector__iterators_8h.html", null ]
    ] ],
    [ "Globals", "globals.html", null ]
  ] ]
];

function createIndent(o,domNode,node,level)
{
  if (node.parentNode && node.parentNode.parentNode)
  {
    createIndent(o,domNode,node.parentNode,level+1);
  }
  var imgNode = document.createElement("img");
  if (level==0 && node.childrenData)
  {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() 
    {
      if (node.expanded) 
      {
        $(node.getChildrenUL()).slideUp("fast");
        if (node.isLast)
        {
          node.plus_img.src = node.relpath+"ftv2plastnode.png";
        }
        else
        {
          node.plus_img.src = node.relpath+"ftv2pnode.png";
        }
        node.expanded = false;
      } 
      else 
      {
        expandNode(o, node, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
  }
  else
  {
    domNode.appendChild(imgNode);
  }
  if (level==0)
  {
    if (node.isLast)
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2plastnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2lastnode.png";
        domNode.appendChild(imgNode);
      }
    }
    else
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2pnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2node.png";
        domNode.appendChild(imgNode);
      }
    }
  }
  else
  {
    if (node.isLast)
    {
      imgNode.src = node.relpath+"ftv2blank.png";
    }
    else
    {
      imgNode.src = node.relpath+"ftv2vertline.png";
    }
  }
  imgNode.border = "0";
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  a.appendChild(node.label);
  if (link) 
  {
    a.href = node.relpath+link;
  } 
  else 
  {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
      node.expanded = false;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() 
  {
    if (!node.childrenUL) 
    {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
}

function expandNode(o, node, imm)
{
  if (node.childrenData && !node.expanded) 
  {
    if (!node.childrenVisited) 
    {
      getNode(o, node);
    }
    if (imm)
    {
      $(node.getChildrenUL()).show();
    } 
    else 
    {
      $(node.getChildrenUL()).slideDown("fast",showRoot);
    }
    if (node.isLast)
    {
      node.plus_img.src = node.relpath+"ftv2mlastnode.png";
    }
    else
    {
      node.plus_img.src = node.relpath+"ftv2mnode.png";
    }
    node.expanded = true;
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) 
  {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
        i==l);
  }
}

function findNavTreePage(url, data)
{
  var nodes = data;
  var result = null;
  for (var i in nodes) 
  {
    var d = nodes[i];
    if (d[1] == url) 
    {
      return new Array(i);
    }
    else if (d[2] != null) // array of children
    {
      result = findNavTreePage(url, d[2]);
      if (result != null) 
      {
        return (new Array(i).concat(result));
      }
    }
  }
  return null;
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;

  getNode(o, o.node);

  o.breadcrumbs = findNavTreePage(toroot, NAVTREE);
  if (o.breadcrumbs == null)
  {
    o.breadcrumbs = findNavTreePage("index.html",NAVTREE);
  }
  if (o.breadcrumbs != null && o.breadcrumbs.length>0)
  {
    var p = o.node;
    for (var i in o.breadcrumbs) 
    {
      var j = o.breadcrumbs[i];
      p = p.children[j];
      expandNode(o,p,true);
    }
    p.itemDiv.className = p.itemDiv.className + " selected";
    p.itemDiv.id = "selected";
    $(window).load(showRoot);
  }
}

