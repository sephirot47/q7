var classgeoc_1_1GeocWidgetInterface =
[
    [ "GeocWidgetInterface", "classgeoc_1_1GeocWidgetInterface.html#a8bfad317ca4d612e0f81581f6d976a40", null ],
    [ "createWidget", "classgeoc_1_1GeocWidgetInterface.html#acf7a40310184e3c40a8c1fa14108d288", null ],
    [ "domXml", "classgeoc_1_1GeocWidgetInterface.html#a4fc16dd113daf1d59af7ea62160054a1", null ],
    [ "group", "classgeoc_1_1GeocWidgetInterface.html#a7999bbe95447bc3c73c0d20a4c699bd5", null ],
    [ "icon", "classgeoc_1_1GeocWidgetInterface.html#ad4fcdb312e1e69b375144fc0bc7b216b", null ],
    [ "includeFile", "classgeoc_1_1GeocWidgetInterface.html#a633db5c29484367224e1510b2494f893", null ],
    [ "isContainer", "classgeoc_1_1GeocWidgetInterface.html#a818f35c84aeb44a87059ddd12a105cb9", null ],
    [ "isInitialized", "classgeoc_1_1GeocWidgetInterface.html#a111e8f8996c4fc49acd0c5972db5d725", null ],
    [ "name", "classgeoc_1_1GeocWidgetInterface.html#a9d3a9ea54c0fdc4a94186fdbadb7899e", null ],
    [ "toolTip", "classgeoc_1_1GeocWidgetInterface.html#a33a15d7bbaae89497d823834057cf273", null ],
    [ "whatsThis", "classgeoc_1_1GeocWidgetInterface.html#a64ae0f75fe710cd6fea0ca5126f774b9", null ]
];