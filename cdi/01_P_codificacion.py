# -*- coding: utf-8 -*-

import random

'''
0. Dada una codificación R, construir un diccionario para codificar m2c y otro para decodificar c2m
'''
R = [('a','0'), ('b','11'), ('c','100'), ('d','1010'), ('e','1011')]

# encoding dictionary
m2c = dict(R)

# decoding dictionary
c2m = dict([(c,m) for m, c in R])


'''
1. Definir una función Encode(M, m2c) que, dado un mensaje M y un diccionario
de codificación m2c, devuelva el mensaje codificado C.
'''


def Encode(M, m2c):
    C = ""
    for c in M:
        C += m2c[c]
    return C


'''
2. Definir una función Decode(C, m2c) que, dado un mensaje codificado C y un diccionario
de decodificación c2m, devuelva el mensaje original M.
'''


def Decode(C, c2m):
    M = ""
    word = ""
    for c in C:
        word += c
        if word in c2m:
            M += c2m[word]
            word = ""
    return M

# test
# print (Decode(Encode("aabbccddedadddaaabbbeeea", m2c), c2m))


#------------------------------------------------------------------------
# Ejemplo 1
#------------------------------------------------------------------------

R = [('a','0'), ('b','11'), ('c','100'), ('d','1010'), ('e','1011')]

# encoding dictionary
m2c = dict(R)

# decoding dictionary
c2m = dict([(c,m) for m, c in R])

'''
3. Generar un mensaje aleatorio M de longitud 50 con las frecuencias
esperadas 50, 20, 15, 10 y 5 para los caracteres
'a', 'b', 'c', 'd', 'e' y codificarlo.
'''

def GetRandMessage(longitude, lettersAndFreqs):
    M = ""
    for i in range(0, longitude):
        accumFreq = 0.0
        r = random.random()
        for (l,f) in lettersAndFreqs:
            accumFreq += f
            if r < accumFreq:
                M += l
                break
    return M

lettersAndFreqs = [('a', 0.5), ('b', 0.2),
                   ('c', 0.15), ('d', 0.1),
                   ('e', 0.05)]
M = GetRandMessage(50, lettersAndFreqs)

C = Encode(M,m2c)

'''
4. Si 'a', 'b', 'c', 'd', 'e' se codifican inicialmente con un código de
bloque de 3 bits, hallar la ratio de compresión al utilizar el nuevo código.
'''

newRatio = 0.0
lengths = map(len, [l[1] for l in R])
for i in range(0, len(lettersAndFreqs)):
    newRatio += lettersAndFreqs[i][1] * lengths[i]

r = 3.0 / newRatio  # ~= 3*len(M) / len(C), para mensajes más grandes
print ("Compression ratio:", r)

#------------------------------------------------------------------------
# Ejemplo 2
#------------------------------------------------------------------------
R = [('a','0'), ('b','10'), ('c','110'), ('d','1110'), ('e','1111')]

# encoding dictionary
m2c = dict(R)

# decoding dictionary
c2m = dict([(c,m) for m, c in R])

'''
5.
Codificar y decodificar 20 mensajes aleatorios de longitudes también aleatorios.
Comprobar si los mensajes decodificados coinciden con los originales.
'''


def GetRandMessage():
    M = ""
    randLongitude = random.randrange(100, 10000)
    for i in range(0, randLongitude):
        randWord = list(m2c.keys())[random.randrange(0,len(m2c))][0]
        M += randWord
    return M


# Tests whether decoding an encoded message returns the original one
def TestMessage(M):
    return (M == Decode(Encode(M, m2c), c2m))


# Test
testGood = False
for i in range(0, 20):
    testGood = TestMessage(GetRandMessage())
    if not testGood:
        break
print ("Test de 20 mensajes aleatorios Passed!" if testGood else "Test de 20 mensajes aleatorios Failed!")


#------------------------------------------------------------------------
# Ejemplo 3
#------------------------------------------------------------------------
R = [('a','0'), ('b','01'), ('c','011'), ('d','0111'), ('e','1111')]

# encoding dictionary
m2c = dict(R)

# decoding dictionary
c2m = dict([(c,m) for m, c in R])

'''
6. Codificar y decodificar los mensajes  'ae' y 'be'.
Comprobar si los mensajes decodificados coinciden con los originales.
'''

print ("'ae' test Passed!" if ( TestMessage('ae') ) else "'ae' test Failed!")
print ("'be' test Passed!" if ( TestMessage('be') ) else "'be' test Failed!")












