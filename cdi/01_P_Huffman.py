# -*- coding: utf-8 -*-

import math
import functools
import numpy as np

print("***********************")

'''
Dada una distribucion de probabilidad, hallar un código de Huffman asociado
'''
PROBS = LEFT  = 0
CODES = RIGHT = 1
def compare(node1, node2): return 1 if ( sum(node1[PROBS]) > sum(node2[PROBS]) ) else -1
def Huffman(ddp):
	ddp_cod = [ [[p], ['']] for p in ddp ]
	while (len(ddp_cod) >= 2):
		ddp_cod.sort(key=functools.cmp_to_key(compare))
		ddp_cod[LEFT ][CODES] = ['0' + c for c in ddp_cod[LEFT ][CODES]] # '0' to the left
		ddp_cod[RIGHT][CODES] = ['1' + c for c in ddp_cod[RIGHT][CODES]] # '1' to the right
		ddp_cod[LEFT][PROBS] += ddp_cod[RIGHT][PROBS] # Merge probs to the left node
		ddp_cod[LEFT][CODES] += ddp_cod[RIGHT][CODES] # Merge codes to the left node
		del ddp_cod[RIGHT]
	result = []
	for i in range(len(ddp_cod[0][PROBS])):
		result.append((ddp_cod[0][PROBS][i], ddp_cod[0][CODES][i]))
	return result

'''
Dada la ddp p=[0.80,0.1,0.05,0.05], hallar un código de Huffman asociado,
la entropía de p y la longitud media de código de Huffman hallado.
'''
def LongitudMedia(probsAndCodes):
        s = 0
        for i in range(len(probsAndCodes)):
                s += len(probsAndCodes[i][CODES]) * probsAndCodes[i][PROBS]
        return s

def H1(p):
        return sum( [0 if (p_i <=  0) else (p_i * -np.log2(p_i)) for p_i in p] )

def informOfDDP(ddp):
	hCodification = Huffman(ddp)
	medLong = LongitudMedia(hCodification)
	entropy = H1( [p for (p,c) in hCodification] )
	print ("---------------------------------")
	print ("Huffman code:   ", hCodification)
	print ("Longitud media: ", medLong)
	print ("Entropia: ", entropy)
	print ("---------------------------------")
	print ("")

ddp = [0.80, 0.1, 0.05, 0.05]
print("Para DDP: ", ddp)
informOfDDP(ddp)

'''
Dada la ddp p=[1/n,..../1/n] con n=2**8, hallar un código de Huffman asociado,
la entropía de p y la longitud media de código de Huffman hallado.
'''
print ("Para DDP: 1/n para ddp=[1/n,..../1/n] con n=2**8")
n = 2 ** 8
p = [1.0 / n for _ in range(n)]
informOfDDP(p)

'''
Dado un mensaje hallar la tabla de frecuencia de los caracteres que lo componen
'''
def tablaFrecuencias(mensaje):
	d = {}
	for c in mensaje:
		d[c] = (d[c]+1) if (c in d) else 1
	msize = len(mensaje)
	return sorted([(symbol, float(n)/msize) for (symbol, n) in d.items()])

'''
Definir una función que codifique un mensaje utilizando un código de Huffman 
obtenido a partir de las frecuencias de los caracteres del mensaje.

Definir otra función que decodifique los mensajes codificados con la función 
anterior.
'''

def EncodeHuffman(mensaje_a_codificar):
	freqTable = tablaFrecuencias(mensaje_a_codificar)
	huffmanEncoding = Huffman( [freq for (letter,freq) in freqTable] )

	huffmanDict = dict() # Recover results from freqs
	for (freq, code) in huffmanEncoding:
		associatedLetterToFreq = ""
		for (letter, freq2) in freqTable:
			if (freq == freq2) and not letter in huffmanDict:
				associatedLetterToFreq = letter
				break
		huffmanDict[associatedLetterToFreq] = code

	mensaje_codificado = ""
	for c in mensaje_a_codificar:
		mensaje_codificado += huffmanDict[c]
	return mensaje_codificado, huffmanDict
    
    
def DecodeHuffman(mensaje_codificado, m2c):
    mensaje_decodificado = ""
    c2m = dict( [(c,m) for m,c in m2c.items()] )
    word = ""
    mensaje_decodificado = ""
    for c in mensaje_codificado:
        word += c
        if word in c2m:
            mensaje_decodificado += c2m[word]
            word = ""
    return mensaje_decodificado
        

'''
Ejemplo
'''
mensaje = 'La heroica ciudad dormía la siesta. El viento Sur, caliente y perezoso, empujaba las nubes blanquecinas que se rasgaban al correr hacia el Norte. En las calles no había más ruido que el rumor estridente de los remolinos de polvo, trapos, pajas y papeles que iban de arroyo en arroyo, de acera en acera, de esquina en esquina revolando y persiguiéndose, como mariposas que se buscan y huyen y que el aire envuelve en sus pliegues invisibles. Cual turbas de pilluelos, aquellas migajas de la basura, aquellas sobras de todo se juntaban en un montón, parábanse como dormidas un momento y brincaban de nuevo sobresaltadas, dispersándose, trepando unas por las paredes hasta los cristales temblorosos de los faroles, otras hasta los carteles de papel mal pegado a las esquinas, y había pluma que llegaba a un tercer piso, y arenilla que se incrustaba para días, o para años, en la vidriera de un escaparate, agarrada a un plomo. Vetusta, la muy noble y leal ciudad, corte en lejano siglo, hacía la digestión del cocido y de la olla podrida, y descansaba oyendo entre sueños el monótono y familiar zumbido de la campana de coro, que retumbaba allá en lo alto de la esbeltatorre en la Santa Basílica. La torre de la catedral, poema romántico de piedra,delicado himno, de dulces líneas de belleza muda y perenne, era obra del siglo diez y seis, aunque antes comenzada, de estilo gótico, pero, cabe decir, moderado por uninstinto de prudencia y armonía que modificaba las vulgares exageraciones de estaarquitectura. La vista no se fatigaba contemplando horas y horas aquel índice depiedra que señalaba al cielo; no era una de esas torres cuya aguja se quiebra desutil, más flacas que esbeltas, amaneradas, como señoritas cursis que aprietandemasiado el corsé; era maciza sin perder nada de su espiritual grandeza, y hasta sussegundos corredores, elegante balaustrada, subía como fuerte castillo, lanzándosedesde allí en pirámide de ángulo gracioso, inimitable en sus medidas y proporciones.Como haz de músculos y nervios la piedra enroscándose en la piedra trepaba a la altura, haciendo equilibrios de acróbata en el aire; y como prodigio de juegosmalabares, en una punta de caliza se mantenía, cual imantada, una bola grande debronce dorado, y encima otra más pequenya, y sobre ésta una cruz de hierro que acababaen pararrayos.'
mensaje_codificado, m2c = EncodeHuffman(mensaje)
mensaje_recuperado = DecodeHuffman(mensaje_codificado, m2c)
print("Mensaje recuperado:\n", mensaje_recuperado)
ratio_compresion = float(8.0 * len(mensaje)) / len(mensaje_codificado)
print("\nCodigo de Huffman usado para comprimir el ultimo mensaje:\n", m2c) 
print("\n[Ratio compresion]: ", ratio_compresion, "\n") # HA DE DAR 1.88
