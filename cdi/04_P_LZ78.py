# -*- coding: utf-8 -*-


"""
Dado un mensaje dar su codificación  usando el
algoritmo LZ78


mensaje='wabba wabba wabba wabba woo woo woo'
LZ78Code(mensaje)=[[0, 'w'], [0, 'a'], [0, 'b'], [3, 'a'], 
                   [0, ' '], [1, 'a'], [3, 'b'], [2, ' '], 
                   [6, 'b'], [4, ' '], [9, 'b'], [8, 'w'], 
                   [0, 'o'], [13, ' '], [1, 'o'], [14, 'w'], 
                   [13, 'o'], [0, 'EOF']]
  
"""
def LZ78Code(mensaje):
	dicStrToIndex = {}
	codification = []
	i = 0
	while i < len(mensaje):
		substr = ""
		j = i
		lastGoodIndexOfSubstrInDic = 0 # 0 means new character
		while (j < len(mensaje)):
			substr += mensaje[j]
			indexOfSubstrInDic = dicStrToIndex[substr] if (substr in dicStrToIndex) else -1
			if (indexOfSubstrInDic == -1): break
			lastGoodIndexOfSubstrInDic = indexOfSubstrInDic
			j += 1

		newDicIndex = len(dicStrToIndex)+1
		if (substr not in dicStrToIndex): # For the last character, to not override its past entry if any
			dicStrToIndex[ substr ] = newDicIndex 
		codification.append(  (lastGoodIndexOfSubstrInDic, substr[-1])  )
		i += j-i+1
	return codification
    
"""
Dado un mensaje codificado con el algoritmo LZ78 hallar el mensaje 
correspondiente 

code=[[0, 'm'], [0, 'i'], [0, 's'], [3, 'i'], [3, 's'], 
      [2, 'p'], [0, 'p'], [2, ' '], [1, 'i'], [5, 'i'], 
      [10, 'p'], [7, 'i'], [0, ' '], [0, 'r'], [2, 'v'], 
      [0, 'e'], [14, 'EOF']]

LZ78Decode(mensaje)='mississippi mississippi river'
"""    
def LZ78Decode(codigo):
	substrings = [""]
	substringsSet = set()
	mensaje = ""
	for (ind, newLetter) in codigo:
		substr = substrings[ind] + newLetter
		if (substr not in substringsSet):
			substringsSet.add(substr)
			substrings.append(substr)
		mensaje += substr
	return mensaje

mensaje='wabba wabba wabba wabba woo woo woo' 
mensaje_codificado=LZ78Code(mensaje)
print('Código: ',mensaje_codificado)   
mensaje_recuperado=LZ78Decode(mensaje_codificado)
print('Código: ',mensaje_codificado)   
print(mensaje)
print(mensaje_recuperado)
if (mensaje!=mensaje_recuperado):
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

mensaje='mississippi mississippi' 
mensaje_codificado=LZ78Code(mensaje)
print('Código: ',mensaje_codificado)   
mensaje_recuperado=LZ78Decode(mensaje_codificado)
print('Código: ',mensaje_codificado)   
print(mensaje)
print(mensaje_recuperado)
if (mensaje!=mensaje_recuperado):
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

'''
mensaje='La heroica ciudad dormía la siesta. El viento Sur, caliente y perezoso, empujaba las nubes blanquecinas que se rasgaban al correr hacia el Norte. En las calles no había más ruido que el rumor estridente de los remolinos de polvo, trapos, pajas y papeles que iban de arroyo en arroyo, de acera en acera, de esquina en esquina revolando y persiguiéndose, como mariposas que se buscan y huyen y que el aire envuelve en sus pliegues invisibles. Cual turbas de pilluelos, aquellas migajas de la basura, aquellas sobras de todo se juntaban en un montón, parábanse como dormidas un momento y brincaban de nuevo sobresaltadas, dispersándose, trepando unas por las paredes hasta los cristales temblorosos de los faroles, otras hasta los carteles de papel mal pegado a las esquinas, y había pluma que llegaba a un tercer piso, y arenilla que se incrustaba para días, o para años, en la vidriera de un escaparate, agarrada a un plomo. Vetusta, la muy noble y leal ciudad, corte en lejano siglo, hacía la digestión del cocido y de la olla podrida, y descansaba oyendo entre sueños el monótono y familiar zumbido de la campana de coro, que retumbaba allá en lo alto de la esbeltatorre en la Santa Basílica. La torre de la catedral, poema romántico de piedra,delicado himno, de dulces líneas de belleza muda y perenne, era obra del siglo diez y seis, aunque antes comenzada, de estilo gótico, pero, cabe decir, moderado por uninstinto de prudencia y armonía que modificaba las vulgares exageraciones de estaarquitectura. La vista no se fatigaba contemplando horas y horas aquel índice depiedra que señalaba al cielo; no era una de esas torres cuya aguja se quiebra desutil, más flacas que esbeltas, amaneradas, como señoritas cursis que aprietandemasiado el corsé; era maciza sin perder nada de su espiritual grandeza, y hasta sussegundos corredores, elegante balaustrada, subía como fuerte castillo, lanzándosedesde allí en pirámide de ángulo gracioso, inimitable en sus medidas y proporciones.Como haz de músculos y nervios la piedra enroscándose en la piedra trepaba a la altura, haciendo equilibrios de acróbata en el aire; y como prodigio de juegosmalabares, en una punta de caliza se mantenía, cual imantada, una bola grande debronce dorado, y encima otra más pequenya, y sobre ésta una cruz de hierro que acababaen pararrayos.'

import time
bits_indice=12
start_time = time.clock()
mensaje_codificado=LZ78Code(mensaje)
print (time.clock() - start_time, "seconds CODE")
start_time = time.clock()
mensaje_recuperado=LZ78Decode(mensaje_codificado)
print (time.clock() - start_time, "seconds DECODE")
ratio_compresion=8*len(mensaje)/((bits_indice+8)*len(mensaje_codificado))
print(len(mensaje_codificado),ratio_compresion)
print("Correcto: ", mensaje==mensaje_recuperado)
if (mensaje!=mensaje_recuperado):
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print(len(mensaje),len(mensaje_recuperado))
        print(mensaje[-5:],mensaje_recuperado[-5:])
 '''
code = [[0, 'W'], [0, 'h'], [0, 'a'], [0, 't'], [0, ' '], [0, 'c'], [0, 'o'], [0, 'u'], [0, 'n'], [4, 's'], [5, 'i'], [0, 's'], [5, 'n'], [7, 't'], [5, 'w'], [2, 'a'], [4, ' '], [12, 'o'], [8, 'n'], [0, 'd'], [12, ' '], [0, 'p'], [0, 'l'], [3, 'u'], [12, 'i'], [0, 'b'], [23, 'e'], [0, ','], [13, 'o'], [17, 'w'], [16, 't'], [15, 'e'], [15, 'o'], [8, 'l'], [20, ' '], [23, 'i'], [0, 'k'], [0, 'e'], [5, 't'], [7, ' '], [26, 'e'], [36, 'e'], [0, 'v'], [38, ','], [29, 't'], [15, 'h'], [3, 't'], [5, 'o'], [9, 'e'], [48, 'r'], [39, 'w'], [40, 'w'], [0, 'i'], [4, 'n'], [38, 's'], [12, 'e'], [21, 'c'], [23, 'a'], [53, 'm'], [28, ' '], [26, 'u'], [17, 'o'], [9, 'l'], [0, 'y'], [46, 'a'], [17, 'i'], [21, 's'], [8, 'p'], [22, 'o'], [0, 'r'], [4, 'e'], [35, 'b'], [64, ' '], [16, 'r'], [35, 'e'], [43, 'i'], [20, 'e'], [9, 'c'], [38, ' '], [70, 'i'], [0, 'g'], [7, 'r'], [7, 'u'], [12, 'l'], [73, 'a'], [9, 'd'], [5, 's'], [37, 'e'], [22, 't'], [53, 'c'], [3, 'l'], [23, 'y'], [5, 'e'], [0, 'x'], [3, 'm'], [53, 'n'], [38, 'd'], [0, '.'], [5, 'E'], [94, 't'], [70, 'a'], [82, 'd'], [96, 'a'], [70, 'y'], [5, 'c'], [58, 'i'], [0, 'm'], [21, 'r'], [38, 'q'], [8, 'i'], [70, 'e'], [93, 'x'], [4, 'r'], [3, 'o'], [70, 'd'], [103, 'r'], [73, 'e'], [76, 'd'], [38, 'n'], [6, 'e'], [98, 'EOF']]

print(LZ78Decode(code))

ases = 1
lastLen = 1
while (True):
	mensaje = ''.join(['A'] * ases)
	L = len(LZ78Code(mensaje))
	print(ases, ": ", L, float(ases)/L, L/float(lastLen))
	lastLen = L
	ases *= 10


