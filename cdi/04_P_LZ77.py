# -*- coding: utf-8 -*-


"""
Dado un mensaje, el tamaño de la ventana de trabajo W, y el tamaño
del buffer de búsqueda S dar la codificación del mensaje usando el
algoritmo LZ77


mensaje='cabracadabrarrarr'
LZ77Code(mensaje,12,18)=[[0, 0, 'c'], [0, 0, 'a'], 
  [0, 0, 'b'], [0, 0, 'r'], [3, 1, 'c'], [2, 1, 'd'], 
  [7, 4, 'r'], [3, 4, 'EOF']]
  
"""

# S = ventana busqueda hacia atras    [AAABBBCCCAAA] | CBCBAC  = 12
# W = toda la window                  [AAABBBCCCAAA  | CBCBAC] = 18
def LZ77Code(mensaje, S = 12, W = 18):
	LookAheadLength = W-S
	SearchBufferLength = S
	mensajeLen = len(mensaje)
	result = []
	i = 0
	while (i < mensajeLen):
		
		searchBegin = max(0, i - SearchBufferLength)
		searchEnd   = i 
		searchStr   = mensaje[ searchBegin : searchEnd ]

		lookAheadBegin = i
		lookAheadEnd   = min(i + LookAheadLength + 1, mensajeLen)
		lookAheadStr   = mensaje[ lookAheadBegin : lookAheadEnd ]
		
		window = searchStr + lookAheadStr
		
		longestMatchLength = 0
		longestMatchIndexInSearchStr = -1 
		for searchBeginIndex in reversed( range(0, len(searchStr)) ):
			matchLength = 0
			for j in range(0, len(window)): 
				if (searchBeginIndex + j >= len(window)): break
				if (j >= len(lookAheadStr)): break
				if (window[searchBeginIndex + j] == lookAheadStr[j]): matchLength += 1
				else: break;
			if (matchLength > longestMatchLength):
				longestMatchLength = matchLength
				longestMatchIndexInSearchStr = searchBeginIndex

		if (longestMatchLength == 0):
			result.append([0, 0, mensaje[i]])
		else:
			foundMatch = window[searchBegin + longestMatchIndexInSearchStr : searchBegin + longestMatchIndexInSearchStr + longestMatchLength]
			i += longestMatchLength 
			character = mensaje[i] if (i < len(mensaje)) else 'EOF'
			result.append([len(searchStr) - longestMatchIndexInSearchStr, longestMatchLength, character])

		i += 1 # Slide the window

	return result

mensaje='cabracadabrarrarr'
mensaje='cabracadabrarrarrabcdefgabcdefgabcdefg'
#print(LZ77Code(mensaje,12,18))
#print(LZ77Code(mensaje,7,13))
#print(LZ77Code("BANANE", 4,7))

"""
Dado un mensaje codificado con el algoritmo LZ77 hallar el mensaje 
correspondiente 

code=[[0, 0, 'p'], [0, 0, 'a'], [0, 0, 't'], [2, 1, 'd'], 
      [0, 0, 'e'], [0, 0, 'c'], [4, 1, 'b'], [0, 0, 'r'], [3, 1, 'EOF']]

LZ77Decode(mensaje)='patadecabra'
"""   
def LZ77Decode(codigo):
	result = ""
	for tupla in codigo:
		pos    = tupla[0] 
		length = tupla[1] 
		nextLetter = tupla[2]

		if (length > 0):
			begin = len(result) - pos
			for i in range(0, length):
				c = result[begin + i]
		#		print ("Adding to result:",c)
				result += c 
		#print ("New letter:", nextLetter)
		if (nextLetter == 'EOF'): break
		result += nextLetter
	return result
'''
msg = 'cabracadabrarrarr'
code = LZ77Code(msg, 12, 18); print(code)
print( LZ77Decode(code) == msg)
msg = 'cabracadabrarrarraaaaaaaaaaaaaaa'; print( LZ77Decode(LZ77Code(msg,  7, 13)) == msg )
msg = 'cabracadabrarrarrabcdabcdabcdabcdabrabcd'; print( LZ77Decode(LZ77Code(msg, 15, 20)) == msg )
msg = 'asdfgasdfgasdfg'; print( LZ77Decode(LZ77Code(msg, 1, 2)) == msg)
print( LZ77Decode(LZ77Code(msg, 1, 2)) )
'''
    
    


"""
Jugar con los valores de S y W (bits_o y bits_l)
para ver sus efectos (tiempo, tamaño...)
"""


mensaje='La heroica ciudad dormía la siesta. El viento Sur, caliente y perezoso, empujaba las nubes blanquecinas que se rasgaban al correr hacia el Norte. En las calles no había más ruido que el rumor estridente de los remolinos de polvo, trapos, pajas y papeles que iban de arroyo en arroyo, de acera en acera, de esquina en esquina revolando y persiguiéndose, como mariposas que se buscan y huyen y que el aire envuelve en sus pliegues invisibles. Cual turbas de pilluelos, aquellas migajas de la basura, aquellas sobras de todo se juntaban en un montón, parábanse como dormidas un momento y brincaban de nuevo sobresaltadas, dispersándose, trepando unas por las paredes hasta los cristales temblorosos de los faroles, otras hasta los carteles de papel mal pegado a las esquinas, y había pluma que llegaba a un tercer piso, y arenilla que se incrustaba para días, o para años, en la vidriera de un escaparate, agarrada a un plomo. Vetusta, la muy noble y leal ciudad, corte en lejano siglo, hacía la digestión del cocido y de la olla podrida, y descansaba oyendo entre sueños el monótono y familiar zumbido de la campana de coro, que retumbaba allá en lo alto de la esbeltatorre en la Santa Basílica. La torre de la catedral, poema romántico de piedra,delicado himno, de dulces líneas de belleza muda y perenne, era obra del siglo diez y seis, aunque antes comenzada, de estilo gótico, pero, cabe decir, moderado por uninstinto de prudencia y armonía que modificaba las vulgares exageraciones de estaarquitectura. La vista no se fatigaba contemplando horas y horas aquel índice depiedra que señalaba al cielo; no era una de esas torres cuya aguja se quiebra desutil, más flacas que esbeltas, amaneradas, como señoritas cursis que aprietandemasiado el corsé; era maciza sin perder nada de su espiritual grandeza, y hasta sussegundos corredores, elegante balaustrada, subía como fuerte castillo, lanzándosedesde allí en pirámide de ángulo gracioso, inimitable en sus medidas y proporciones.Como haz de músculos y nervios la piedra enroscándose en la piedra trepaba a la altura, haciendo equilibrios de acróbata en el aire; y como prodigio de juegosmalabares, en una punta de caliza se mantenía, cual imantada, una bola grande debronce dorado, y encima otra más pequenya, y sobre ésta una cruz de hierro que acababaen pararrayos. La heroica ciudad dormía la siesta. El viento Sur, caliente y perezoso, empujaba las nubes blanquecinas que se rasgaban al correr hacia el Norte. En las calles no había más ruido que el rumor estridente de los remolinos de polvo, trapos, pajas y papeles que iban de arroyo en arroyo, de acera en acera, de esquina en esquina revolando y persiguiéndose, como mariposas que se buscan y huyen y que el aire envuelve en sus pliegues invisibles. Cual turbas de pilluelos, aquellas migajas de la basura, aquellas sobras de todo se juntaban en un montón, parábanse como dormidas un momento y brincaban de nuevo sobresaltadas, dispersándose, trepando unas por las paredes hasta los cristales temblorosos de los faroles, otras hasta los carteles de papel mal pegado a las esquinas, y había pluma que llegaba a un tercer piso, y arenilla que se incrustaba para días, o para años, en la vidriera de un escaparate, agarrada a un plomo. Vetusta, la muy noble y leal ciudad, corte en lejano siglo, hacía la digestión del cocido y de la olla podrida, y descansaba oyendo entre sueños el monótono y familiar zumbido de la campana de coro, que retumbaba allá en lo alto de la esbeltatorre en la Santa Basílica. La torre de la catedral, poema romántico de piedra,delicado himno, de dulces líneas de belleza muda y perenne, era obra del siglo diez y seis, aunque antes comenzada, de estilo gótico, pero, cabe decir, moderado por uninstinto de prudencia y armonía que modificaba las vulgares exageraciones de estaarquitectura. La vista no se fatigaba contemplando horas y horas aquel índice depiedra que señalaba al cielo; no era una de esas torres cuya aguja se quiebra desutil, más flacas que esbeltas, amaneradas, como señoritas cursis que aprietandemasiado el corsé; era maciza sin perder nada de su espiritual grandeza, y hasta sussegundos corredores, elegante balaustrada, subía como fuerte castillo, lanzándosedesde allí en pirámide de ángulo gracioso, inimitable en sus medidas y proporciones.Como haz de músculos y nervios la piedra enroscándose en la piedra trepaba a la altura, haciendo equilibrios de acróbata en el aire; y como prodigio de juegosmalabares, en una punta de caliza se mantenía, cual imantada, una bola grande debronce dorado, y encima otra más pequenya, y sobre ésta una cruz de hierro que acababaen pararrayos. La heroica ciudad dormía la siesta. El viento Sur, caliente y perezoso, empujaba las nubes blanquecinas que se rasgaban al correr hacia el Norte. En las calles no había más ruido que el rumor estridente de los remolinos de polvo, trapos, pajas y papeles que iban de arroyo en arroyo, de acera en acera, de esquina en esquina revolando y persiguiéndose, como mariposas que se buscan y huyen y que el aire envuelve en sus pliegues invisibles. Cual turbas de pilluelos, aquellas migajas de la basura, aquellas sobras de todo se juntaban en un montón, parábanse como dormidas un momento y brincaban de nuevo sobresaltadas, dispersándose, trepando unas por las paredes hasta los cristales temblorosos de los faroles, otras hasta los carteles de papel mal pegado a las esquinas, y había pluma que llegaba a un tercer piso, y arenilla que se incrustaba para días, o para años, en la vidriera de un escaparate, agarrada a un plomo. Vetusta, la muy noble y leal ciudad, corte en lejano siglo, hacía la digestión del cocido y de la olla podrida, y descansaba oyendo entre sueños el monótono y familiar zumbido de la campana de coro, que retumbaba allá en lo alto de la esbeltatorre en la Santa Basílica. La torre de la catedral, poema romántico de piedra,delicado himno, de dulces líneas de belleza muda y perenne, era obra del siglo diez y seis, aunque antes comenzada, de estilo gótico, pero, cabe decir, moderado por uninstinto de prudencia y armonía que modificaba las vulgares exageraciones de estaarquitectura. La vista no se fatigaba contemplando horas y horas aquel índice depiedra que señalaba al cielo; no era una de esas torres cuya aguja se quiebra desutil, más flacas que esbeltas, amaneradas, como señoritas cursis que aprietandemasiado el corsé; era maciza sin perder nada de su espiritual grandeza, y hasta sussegundos corredores, elegante balaustrada, subía como fuerte castillo, lanzándosedesde allí en pirámide de ángulo gracioso, inimitable en sus medidas y proporciones.Como haz de músculos y nervios la piedra enroscándose en la piedra trepaba a la altura, haciendo equilibrios de acróbata en el aire; y como prodigio de juegosmalabares, en una punta de caliza se mantenía, cual imantada, una bola grande debronce dorado, y encima otra más pequenya, y sobre ésta una cruz de hierro que acababaen pararrayos. La heroica ciudad dormía la siesta. El viento Sur, caliente y perezoso, empujaba las nubes blanquecinas que se rasgaban al correr hacia el Norte. En las calles no había más ruido que el rumor estridente de los remolinos de polvo, trapos, pajas y papeles que iban de arroyo en arroyo, de acera en acera, de esquina en esquina revolando y persiguiéndose, como mariposas que se buscan y huyen y que el aire envuelve en sus pliegues invisibles. Cual turbas de pilluelos, aquellas migajas de la basura, aquellas sobras de todo se juntaban en un montón, parábanse como dormidas un momento y brincaban de nuevo sobresaltadas, dispersándose, trepando unas por las paredes hasta los cristales temblorosos de los faroles, otras hasta los carteles de papel mal pegado a las esquinas, y había pluma que llegaba a un tercer piso, y arenilla que se incrustaba para días, o para años, en la vidriera de un escaparate, agarrada a un plomo. Vetusta, la muy noble y leal ciudad, corte en lejano siglo, hacía la digestión del cocido y de la olla podrida, y descansaba oyendo entre sueños el monótono y familiar zumbido de la campana de coro, que retumbaba allá en lo alto de la esbeltatorre en la Santa Basílica. La torre de la catedral, poema romántico de piedra,delicado himno, de dulces líneas de belleza muda y perenne, era obra del siglo diez y seis, aunque antes comenzada, de estilo gótico, pero, cabe decir, moderado por uninstinto de prudencia y armonía que modificaba las vulgares exageraciones de estaarquitectura. La vista no se fatigaba contemplando horas y horas aquel índice depiedra que señalaba al cielo; no era una de esas torres cuya aguja se quiebra desutil, más flacas que esbeltas, amaneradas, como señoritas cursis que aprietandemasiado el corsé; era maciza sin perder nada de su espiritual grandeza, y hasta sussegundos corredores, elegante balaustrada, subía como fuerte castillo, lanzándosedesde allí en pirámide de ángulo gracioso, inimitable en sus medidas y proporciones.Como haz de músculos y nervios la piedra enroscándose en la piedra trepaba a la altura, haciendo equilibrios de acróbata en el aire; y como prodigio de juegosmalabares, en una punta de caliza se mantenía, cual imantada, una bola grande debronce dorado, y encima otra más pequenya, y sobre ésta una cruz de hierro que acababaen pararrayos.'
# Jugar con valores de S y W:

mensaje = ''.join(['A'] * 10000)
#print(mensaje)
bestRatio = 0.05
bestBits_o = 0
bestBits_l = 0
bits_l = 8
#for bits_l in range(8):
bits_o = bits_l
while (bits_o >= 0):
	W = 2**bits_l
	S = 2**bits_o

	print ("Compresion con S =", S, " y W =", W)
	import time
	start_time = time.clock()
	mensaje_codificado=LZ77Code(mensaje,S,W)
	print (time.clock() - start_time, "seconds code")
	start_time = time.clock()
	mensaje_recuperado=LZ77Decode(mensaje_codificado)
	print ("Correcto: ", mensaje==mensaje_recuperado)
	print (time.clock() - start_time, "seconds decode")
	ratio_compresion=8*len(mensaje)/((bits_o+bits_l+8)*len(mensaje_codificado)) # Ha de dar (ratio = 3.6 y len = 727), con bits_o=14, bits_l=6 CORRECTO!
	if (ratio_compresion > bestRatio):
		bestRatio, bestBits_o, bestBits_l = ratio_compresion, bits_o, bits_l
	print('Longitud de mensaje codificado:', len(mensaje_codificado))
	print('Ratio de compresión:', ratio_compresion)
	print("-------------------------------------------------------------")
	bits_o -= 1


print ("***************************************")
print ("MEJOR RATIO DE COMPRESION: ")
print ("Ratio:", bestRatio)
print ("bits_o:", bestBits_o, "(S = ", (2**bestBits_o), ")")
print ("bits_l:", bestBits_l, "(W = ", (2**bestBits_l), ")")

bits_o=14
bits_l=6
S=2**bits_o
W=2**bits_o+2**bits_l

msg = LZ77Decode([[0,0,'t'], [0,0,'e'], [0,0,'l'], [2,3,'t'], [6,5,'EOF']])
print(msg)

code = LZ77Code('TELELELELELE')
print(code)

redecode = LZ77Decode(code)
print(redecode)
