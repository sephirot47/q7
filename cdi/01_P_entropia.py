# -*- coding: utf-8 -*-
"""

"""
import math
import numpy as np
import matplotlib.pyplot as plt


'''
Dada una lista p, decidir si es una distribución de probabilidad (ddp)
0<=p[i]<=1, sum(p[i])=1.
'''
def es_ddp(p,tolerancia=10**(-5)):
	return (abs(1.0-sum(p)) < tolerancia) and \
		all( (pi>=0 and pi<=1) for pi in p )

'''
Dado un código C y una ddp p, hallar la longitud media del código.
'''
def LongitudMedia(C,p):
	s = 0
	for i in range(len(C)):
		s += len(C[i]) * p[i]
	return s

    
'''
Dada una ddp p, hallar su entropía.
'''
def H1(p):
	return sum( [0 if (p_i <=  0) else (p_i * -np.log2(p_i)) for p_i in p] )	


'''
Dada una lista de frecuencias n, hallar su entropía.
'''
def H2(n):
	totalFreq = float(sum(n))
	return 0 if totalFreq == 0 else H1( [f/totalFreq for f in n] )



'''
Ejemplos
'''
C=['001','101','11','0001','000000001','0001','0000000000']
p=[0.5,0.1,0.1,0.1,0.1,0.1,0]
n=[5,2,1,1,1]

print(H1(p))
print(H2(n))
print(LongitudMedia(C,p))



'''
Dibujar H(p,1-p)
'''
pvalues = [p for p in np.arange(0, 1, 0.005)]
x = [p            for p in pvalues]
y = [H1([p, 1-p]) for p in pvalues]
plt.plot(x, y)
plt.show()


'''
Hallar aproximadamente el máximo de  H(p,q,1-p-q)
'''
pvalues = [p for p in np.arange(0, 1, 0.004)]
p_max = q_max = h_max = 0
for p in pvalues:
	for q in pvalues:
		h = H1([p,q,1-p-q])
		if (h > h_max):
			p_max, q_max, h_max = p, q, h
print ("El maximo es (p=" + str(p_max) +  ", q=" + str(q_max) +  ", h=" + str(h_max) + ") aproximadamente")

'''
#Tests privados

print "-----------------------------"

# es_ddp
print "es_ddp:"
print es_ddp([0.1, 0.2, 0.3, 0.4]) # True
print es_ddp([0.1, 0.2, 0.3, 0.5]) # False
print es_ddp([0.3, 0.4, 0.2])      # False
print es_ddp([0.8, 0.4, -0.2])     # False
print es_ddp([1.1])     	   # False
print es_ddp([1.0])                # True

# Longitud media
print "Longitudes medias:"
print LongitudMedia(['00', '010', '011'], [0.5, 0.3, 0.2]) #2.5
print LongitudMedia(['010', '011', '00'], [0.5, 0.3, 0.2]) #2.8
print LongitudMedia(['001','101','11','0001','000000001','0001','0000000000'], 
		    [0.5, 0.1, 0.1, 0.1, 0.1, 0.1, 0])     #3.7

# Entropia de ddp
print "Entropia ddp"
print H1([0.5, 0.1, 0.1, 0.1, 0.1, 0.1, 0]) #2.16
print H1([0.5, 0.5]) #1
print H1([1]) #0
print H1([15.0/16, 1.0/16]) #~0.3372

# Entropia de freqs
print "Entropia freqs"
print H2([0]) #0
print H2([50, 10, 10, 10, 10, 10, 0]) #2.16
print H2([47, 47]) #1
print H2([100]) #0
print H2([15, 1]) #~0.3372

print "-----------------------------"
'''
