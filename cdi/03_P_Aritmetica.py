# -*- coding: utf-8 -*-


import math
import random

"""
Dado x en [0,1) dar su representacion en binario, por ejemplo
dec2bin(0.625)='101'
dec2bin(0.0625)='0001'

Dada la representación binaria de un real perteneciente al intervalo [0,1) 
dar su representación en decimal, por ejemplo

bin2dec('101')=0.625
bin2dec('0001')=0.0625

nb número máximo de bits
"""

def dec2bin(x, nb=100):
	result = ''
	for _ in range(nb):
		x *= 2
		if (x >= 1): 
			result += '1'
			x -= 1
		else: result += '0'
		if (x == 0): break
	return result
		
def bin2dec(xb):
	num = 0
	step = 0.5
	for b in xb:
		num += 0 if b == '0' else step
		step *= 0.5
	return num


"""
Dada una distribución de probabilidad p(i), i=1..n,
hallar su función distribución:
f(0)=0
f(i)=sum(p(k),k=1..i).
"""

def cdf(ddp):
	fdd = []
	accum = 0
	for p in ddp:
		accum += p
		fdd.append(accum)
	return fdd

def lerp(ddd, oldl, oldu, newl, newu):
	return [(d - oldl) * (newu - newl) / (oldu - oldl) + newl for d in ddd]
	

"""
Dado un mensaje y su alfabeto con su distribución de probabilidad
dar el intervalo (l,u) que representa al mensaje.

mensaje  = 'ccda'
alfabeto = ['a', 'b', 'c', 'd']
probabilidades = [0.4, 0.3, 0.2, 0.1]
Arithmetic(mensaje, alfabeto, probabilidades) = 0.876 0.8776
"""

def Arithmetic(mensaje, alfabeto, probabilidades):
	probabilidades = [0] + probabilidades
	ddd = cdf(probabilidades)
	l = 0 ; u = 1
	for c in mensaje:
		didx = alfabeto.index(c) + 1
		oldl, oldu = l, u
		l = ddd[didx-1] if (didx > 0) else l
		u = ddd[didx]
		ddd = lerp(ddd, oldl, oldu, l, u)
	return l, u

"""
Dado un mensaje y su alfabeto con su distribución de probabilidad
dar la representación binaria de x=r/2**(t) siendo t el menor 
entero tal que 1/2**(t)<l-u, r entero (si es posible par) tal 
que l*2**(t)<=r<u*2**(t)

mensaje='ccda'
alfabeto=['a','b','c','d']
probabilidades=[0.4,0.3,0.2,0.1]
EncodeArithmetic1(mensaje,alfabeto,probabilidades)='111000001'
"""

def EncodeArithmetic1(mensaje, alfabeto, probabilidades):
	l, u = Arithmetic(mensaje, alfabeto, probabilidades)
	t = math.floor( -math.log2(u-l) )
	lr = math.ceil (2**t * l)
	ur = math.floor(2**t * u)
	r = lr if (lr==ur) else (lr if (lr%2==0) else lr+1) # Cogemos x par si es posible
	x = r/(2**t)
	return dec2bin(x)


"""
Dado un mensaje y su alfabeto con su distribución de probabilidad
dar el código que representa el mensaje obtenido a partir de la 
representación binaria de l y u

mensaje='ccda'
alfabeto=['a','b','c','d']
probabilidades=[0.4,0.3,0.2,0.1]
EncodeArithmetic2(mensaje,alfabeto,probabilidades)='111000001'
"""
def EncodeArithmetic2(mensaje, alfabeto, probabilidades):
	l, u = Arithmetic(mensaje, alfabeto, probabilidades)
	lbin = dec2bin(l)
	ubin = dec2bin(u)
	for i in range(min(len(lbin), len(ubin))):
                cl, cu = lbin[i], ubin[i]
                if (i == len(lbin)-1 or cl != cu):
                        a1_ar = lbin[0:i] 
                        if   (len(lbin) == len(a1_ar)): # l = 0,a1...ar
                                return a1_ar
                        else: #(len(ubin) >  len(a1_ar)): # u > 0,a1...ar1 
                                return a1_ar + '1' 

"""
Dada la representación binaria del número que representa un mensaje, la
longitud del mensaje y el alfabeto con su distribución de probabilidad 
dar el mensaje original

code='0'
longitud=4
alfabeto=['a','b','c','d']
probabilidades=[0.4,0.3,0.2,0.1]
DecodeArithmetic(code,longitud,alfabeto,probabilidades)='aaaa'

code='111000001'
DecodeArithmetic(code,4,alfabeto,probabilidades)='ccda'
DecodeArithmetic(code,5,alfabeto,probabilidades)='ccdab'

"""

def DecodeArithmetic(code, n, alfabeto, probabilidades):
	probabilidades = [0] + probabilidades
	ddd = cdf(probabilidades)
	number = bin2dec(code) 
	result = ''
	for j in range(n):
		for i in range(len(ddd)):
			if number < ddd[i]:
				result += alfabeto[i-1]
				break
		l = ddd[i-1] if i > 0 else 0
		u = ddd[i]
		ddd = lerp(ddd, ddd[0], ddd[-1], l, u)
	return result	

'''
Función que compara la longitud esperada del 
mensaje con la obtenida con la codificación aritmética
'''

def comparacion(mensaje, alfabeto, probabilidades):
    p=1.
    indice=dict([(alfabeto[i],i+1) for i in range(len(alfabeto))])
    for i in range(len(mensaje)):
        p=p*probabilidades[indice[mensaje[i]]-1]
    e1 = EncodeArithmetic1(mensaje,alfabeto,probabilidades)
    e2 = EncodeArithmetic2(mensaje,alfabeto,probabilidades)
    print("EA1 & dec.:", DecodeArithmetic(e1, len(mensaje), alfabeto, probabilidades))
    print("EA2 & dec.:", DecodeArithmetic(e2, len(mensaje), alfabeto, probabilidades))
    aux=-math.log(p,2), len(e1), len(e2)
    print('Información y longitudes:',aux)    
    return aux
        
        
'''
Generar 10 mensajes aleatorios M de longitud 10<=n<=20 aleatoria 
con las frecuencias esperadas 50, 20, 15, 10 y 5 para los caracteres
'a', 'b', 'c', 'd', 'e', codificarlo y compararlas longitudes 
esperadas con las obtenidas.
'''

alfabeto=['a','b','c','d','e']
probabilidades=[0.5,0.2,0.15,0.1,.05]
U = 50*'a'+20*'b'+15*'c'+10*'d'+5*'e'
def rd_choice(X,k = 1):
    Y = []
    for _ in range(k):
        Y +=[random.choice(X)]
    return Y

l_max=20


msg = 'ababcd'
alfabeto =  ['a','b','c','d']
probs = [0.25, 0.25, 0.25, 0.25]

code1 = EncodeArithmetic1(msg, alfabeto, probs)
print(bin2dec(code1))

code2 = EncodeArithmetic2(msg, alfabeto, probs)
print(bin2dec(code2))

longitud=6
print(DecodeArithmetic(code1,longitud,alfabeto,probs))
print(DecodeArithmetic(code2,longitud,alfabeto,probs))

print(DecodeArithmetic(dec2bin(0.15), 6, alfabeto, probs))

code3 = EncodeArithmetic2('acbcbc', alfabeto, probs)
print(bin2dec(code3))
'''
for _ in range(10):
    n=random.randint(10,l_max)
    L = rd_choice(U, n)
    mensaje = ''
    for x in L:
        mensaje += x
    print('---------- ',mensaje)    
    comparacion(mensaje,alfabeto,probabilidades)
        
Generar 10 mensajes aleatorios M de longitud 10<=n<=100 aleatoria 
con las frecuencias esperadas 50, 20, 15, 10 y 5 para los caracteres
'a', 'b', 'c', 'd', 'e' y codificarlo.
alfabeto=['a','b','c','d','e']
probabilidades=[0.5,0.2,0.15,0.1,.05]
U = 50*'a'+20*'b'+15*'c'+10*'d'+5*'e'
def rd_choice(X,k = 1):
    Y = []
    for _ in range(k):
        Y +=[random.choice(X)]
    return Y

l_max=100

for _ in range(10):
    n=random.randint(10,l_max)
    L = rd_choice(U, n)
    mensaje = ''
    for x in L:
        mensaje += x
    print('---------- ',mensaje)    
    C = EncodeArithmetic1(mensaje,alfabeto,probabilidades)
    print(C)

'''
