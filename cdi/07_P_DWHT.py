# -*- coding: utf-8 -*-

########################################################

import functools
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


# Funciones para obtener la DWHT, y ordenar las filas por # cambio de signo
def dwht(n, rec=False):
	if   (n == 1): return [[1]]
	elif (n == 2): return [[1,1], [1,-1]]
	HW = np.array(dwht(n/2, True))
	result = np.empty([n, n])
	for y in range(0, int(n)):
		mult = 1 if y < n/2 else -1
		r = np.empty([0]);
		r = np.append(r,        HW[y % (n/2)]);
		r = np.append(r, mult * HW[y % (n/2)])
		result[y] = r
	return (1.0 if rec else 1.0/np.sqrt(n)) * np.array(result)

def countSignChanges(row):
	n = 0
	prev = row[0] 
	for i in range(len(row)):
		if (prev * row[i] < 0): n += 1
		prev = row[i]
	return n

def compare(row1, row2): return 1 if ( countSignChanges(row1) > countSignChanges(row2) ) else -1
def orderRowsBySign(matrix):
	nrows = np.array(matrix).shape[0]
	rows = []
	for i in range(0, nrows): rows.append(matrix[i])
	rows.sort(key=functools.cmp_to_key(compare))
	result = np.array(rows)
	return result

# Algunas matrices predefinidas
HWH16 = orderRowsBySign(dwht(16))
HWH8  = orderRowsBySign(dwht(8))
HWH4  = orderRowsBySign(dwht(4))
HWH2  = orderRowsBySign(dwht(2))

'''
Implementar la DWHT Discrete Walsh-Hadamard Transform y su inversa
para bloques NxN 

dwht_bloque(p,HWH,N) 
idwht_bloque(p,HWH,N) 

p bloque NxN
HWH matriz de la transformación
'''

def dwht_bloque(p, HWH=HWH8, n_bloque=8):
	result = np.tensordot(HWH, p, axes=[1,0])
	result = np.tensordot(HWH, result, axes=[0,1]) 
	return result

def idwht_bloque(p, HWH=HWH8, n_bloque=8):
	return dwht_bloque(p, HWH, n_bloque)

"""
# Inverse test
X = [[ 0.25,  0.25, -0.25, -0.25],
     [ 0.25,  0.25, -0.25, -0.25],
     [-0.25, -0.25,  0.25,  0.25],
     [-0.25, -0.25,  0.25,  0.25]]
print ( idwht_bloque(X, HWH4, 4) )
"""

'''
Reproducir los bloques base de la transformación para los casos N=4,8 (Ver imágenes adjuntas)
'''

def drawBlocks(N):
	HWH = orderRowsBySign(dwht(N))
	fig = plt.figure()
	gs = gridspec.GridSpec(N, N)
	for i in range(0, N):
		for j in range(0, N):
			B = np.zeros( (N, N) )
			B[i][j] = 1
			B = dwht_bloque(B, HWH, N)
			plt.subplot(N, N, (j * N) + i + 1)
			plt.imshow(B); plt.xticks([]) ; plt.yticks([]) ; 
	plt.show()

#drawBlocks(16)
drawBlocks(8)
drawBlocks(4)

