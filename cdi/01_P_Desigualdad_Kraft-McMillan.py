# -*- coding: utf-8 -*-
"""

"""

def getKraftCoeff(L, r=2):
	suma = 0
	for i in range(0, len(L)):
		suma += 1.0/pow(r, L[i])
	return suma
'''
Dada la lista L de longitudes de las palabras de un código
r-ario, decidir si pueden definir un código.

'''

def  kraft1(L, r=2): # Usando la Desigualdad de Kraft
	return getKraftCoeff(L,r) <= 1


'''
Dada la lista L de longitudes de las palabras de un código
r-ario, calcular el máximo número de palabras de longitud
máxima, max(L), que se pueden añadir y seguir siendo un código.

'''

def  kraft2(L, r=2):
    maxWordLong = max(L)
    kc = getKraftCoeff(L,r)
    return max(0, int((1.0-kc) * pow(r, maxWordLong)))


'''
Dada la lista L de longitudes de las palabras de un
código r-ario, calcular el máximo número de palabras
de longitud Ln, que se pueden añadir y seguir siendo
un código.
'''

def  kraft3(L, Ln, r=2):
    kc = getKraftCoeff(L,r)
    return max(0, int((1.0-kc) * pow(r, Ln)))


'''
Dada la lista L de longitudes de las palabras de un
código r-ario, hallar un código prefijo con palabras
con dichas longiutudes
'''

def CodeRec(currentWord, remainingLongitudes, symbolPool, prefixCode):
	if len(remainingLongitudes) == 0 or len(currentWord) >= max(remainingLongitudes):
		return

	for s in symbolPool:
		w = currentWord + s
		if len(w) in remainingLongitudes:
			remainingLongitudes.remove(len(w))
			prefixCode.append(w)
		else:
			CodeRec(w, remainingLongitudes, symbolPool, prefixCode)	

def Code(longitudes, r=2):
	if not kraft1(longitudes, r):
		print ("Impossible to create a prefix code for ", (longitudes, r) )
		return []

	L = longitudes
	symbolPool = [chr(i) for i in range(ord('0'), ord('0') + r)]
	prefixCode = []
	CodeRec("", L, symbolPool, prefixCode)
	return prefixCode

'''
Ejemplo
'''

#L=[1,3,5,5,10,3,5,7,8,9,9]
#print(sorted(L), ' codigo final:', Code(L,3))
#print(kraft1(L))

L = [2,3,4,5,6,6,6]
print(kraft2(L))

#print(kraft1(['0', '10', '110', '111'])
#print(kraft1(['00', '010', '011', '100', '101', '1100', '1101', '1110', '1111', '11111'])
#print(kraft1(['00', '010', '011', '100', '101', '1100', '1101', '1110', '1111'])
#print(kraft1(['00', '01', '100', '101', '110', '111', '1111'])
print(kraft1([1,2,3,3]))
print(kraft1([2,3,3,3,3,4,4,4,4,5]))
print(kraft1([2,3,3,3,3,4,4,4,4]))
print(kraft1([2,2,3,3,3,3,4]))
print(Code([1,2,3,3]))
print(Code([2,3,3,3,3,4,4,4,4,5]))
print(Code([2,3,3,3,3,4,4,4,4]))
print(Code([2,2,3,3,3,3,4]))
''' Tests privados '''
'''
print("-----------------------")
print("Private tests----------")
print("")
print("kraft1 ----------------")
print(kraft1([1])) # True
print(kraft1([1,2])) # True
print(kraft1([1,2,2])) # True
print(kraft1([1,2,2,2])) # False
print(kraft1([1,2,2,3])) # False
print(kraft1([1,2,3])) # True
print(kraft1([1,2,3,3])) # True
print(kraft1([1,2,3,3,3])) # False
print(kraft1([2,2,2,3,3])) # True
print("")
print("kraft2 ----------------")
print(kraft2([1])) # 1
print(kraft2([1,1])) # 0
print(kraft2([1,2])) # 1
print(kraft2([1,2,5])) # 7
print(kraft2([1,2,6])) # 15
print(kraft2([1,2,2,7])) # 0
print(kraft2([1,2,2,3])) # 0
print(kraft2([1,2,3,4])) # 1
print(kraft2([1,2,3,3])) # 0
print(kraft2([1,2,3,3,3])) # 0
print(kraft2([2,2,2,3,3])) # 0
print(kraft2([1,7,8])) # 125
print(kraft2([1,8])) # 127
print("")
print("kraft3 ----------------")
print(kraft3([1],1)) # 1
print(kraft3([1],2)) # 2
print(kraft3([1,1],2)) # 0
print(kraft3([1,2],2)) # 1
print(kraft3([1,2,5],1)) # 0
print(kraft3([1,2,6],4)) # 3
print(kraft3([1,2,7],9)) # 124
print(kraft3([1,7,8],5)) # 15
print(kraft3([1,8],3)) # 3
print("")
print("Code ----------------")
print(Code([1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8], 8))
print(Code([1,1], 1)) # Impossible
print("")
print("-----------------------")
'''
