# -*- coding: utf-8 -*-

from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

imgOriginal = misc.ascent()           # Leo la imagen
(height, width) = imgOriginal.shape   # Filas y columnas de la imagen
#plt.imshow(imgOriginal, cmap = plt.cm.gray) 
#plt.xticks([]) ; plt.yticks([]) ; plt.show() 
       
def imgInfo(imgOriginal, imgCuantizada):
	sigma = np.sqrt( sum(  sum( (imgOriginal - imgCuantizada)**2 )) ) / (width * height)
	print ("Sigma:", sigma)

##########################################################################
 
"""
Mostrar la imagen habiendo cuantizado los valores de los píxeles en
2**k niveles, k=1..8

Para cada cuantización dar la ratio de compresión y Sigma

Sigma=np.sqrt(sum(sum((imagenOriginal-imagenCuantizada)**2)))/(n*m)
"""

k = 2
b = 2**k
div = 256/b
imgCuantizada = ((imgOriginal // div) * div) + (div // 2)

plt.imshow(imgCuantizada, cmap = plt.cm.gray, vmin = 0, vmax = 255)
plt.xticks([]) ; plt.yticks([]) ; plt.show() 
imgInfo(imgOriginal, imgCuantizada)
ratio_compresion = 8 / k
print ("Compression ratio:", ratio_compresion)

############################################################################

"""
Mostrar la imagen cuantizando los valores de los pixeles de cada bloque
n_bloque x n_bloque en 2^k niveles, siendo n_bloque=8 y k=2

Calcular Sigma y la ratio de compresión (para cada bloque 
es necesario guardar 16 bits extra para los valores máximos 
y mínimos del bloque, esto supone 16/n_bloque**2 bits más por pixel).
"""

k = 2
b = 2**k
n_bloque = 8
imgCuantizada = [[0 for x in range(width)] for y in range(height)]
for x in range(0, width, n_bloque):
	for y in range(0, height, n_bloque):
		minv = maxv = imgOriginal[y][x]
		for bx in range(n_bloque):
			for by in range(n_bloque):
				minv = min(minv, imgOriginal[y+by][x+bx])
				maxv = max(maxv, imgOriginal[y+by][x+bx])
				
		div = max(1, (maxv-minv)//b)
		for bx in range(n_bloque):
			for by in range(n_bloque):
				v = imgOriginal[y+by][x+bx] - minv
				vCuant = int( (v // div) * div ) + (div // 2) + minv
				imgCuantizada[y+by][x+bx] = vCuant

plt.imshow(imgCuantizada, cmap = plt.cm.gray, vmin = 0, vmax = 255)
plt.xticks([]) ; plt.yticks([]) ; plt.show() 
imgInfo(imgOriginal, imgCuantizada)
ratio_compresion = 8 / (k + 16.0 / (n_bloque*n_bloque))
print ("Compression ratio:", ratio_compresion)
 
#######################################################################
