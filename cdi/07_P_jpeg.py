# -*- coding: utf-8 -*-
"""

"""

import time as time
import numpy as np
import scipy
import scipy.ndimage
import math 
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
PI = math.pi



        
"""
Matrices de cuantización, estándares y otras
"""
   
Q_Luminance=np.array([
    [16 ,11, 10, 16,  24,  40,  51,  61],
    [12, 12, 14, 19,  26,  58,  60,  55],
    [14, 13, 16, 24,  40,  57,  69,  56],
    [14, 17, 22, 29,  51,  87,  80,  62],
    [18, 22, 37, 56,  68, 109, 103,  77],
    [24, 35, 55, 64,  81, 104, 113,  92],
    [49, 64, 78, 87, 103, 121, 120, 101],
    [72, 92, 95, 98, 112, 100, 103,  99]])


Q_Chrominance=np.array([
[17, 18, 24, 47, 99, 99, 99, 99],
[18, 21, 26, 66, 99, 99, 99, 99],
[24, 26, 56, 99, 99, 99, 99, 99],
[47, 66, 99, 99, 99, 99, 99, 99],
[99, 99, 99, 99, 99, 99, 99, 99],
[99, 99, 99, 99, 99, 99, 99, 99],
[99, 99, 99, 99, 99, 99, 99, 99],
[99, 99, 99, 99, 99, 99, 99, 99]])
 

def Q_matrix(r=1):
    m=np.zeros((8,8))
    for i in range(8):
        for j in range(8):
            m[i,j]=(1+i+j)*r
    return m

# Funcion que devuelve la matriz de DCT N*N
def getDCT(N):
	# Si hemos calculado anteriormente la matriz DCT, no la recalculamos
	if (N in getDCT.cache): return getDCT.cache[N] 

	dct = np.empty ( (N,N) )
	s2inv = 1/np.sqrt(2)
	for i in range(N):
		Ci = s2inv if i == 0 else 1
		for j in range(N):
			k = i + (2*i*j)
			dct[i][j] = Ci * (np.cos(k * PI / (2*N)))
	result = np.sqrt(2/N) * dct
	getDCT.cache[N] = result
	return result
getDCT.cache = {}

# Funcion que devuelve la inversa de la matriz de DCT N*N
def getDCTInv(N):
	# Si hemos calculado anteriormente la inversa de la matriz DCT, no la recalculamos
	if (N in getDCTInv.cache): return getDCTInv.cache[N] 
	result = getDCT(N).T
	return result
getDCTInv.cache = {}

"""
Implementar la DCT (Discrete Cosine Transform) 
y su inversa para bloques NxN

dct_bloque(p,N)
idct_bloque(p,N)

p bloque NxN
"""

def dct_bloque(p,N):
	DCT = getDCT(N)
	result = np.tensordot(DCT, p, axes=[1,0])
	result = np.tensordot(DCT, result.T, axes=[1,0])
	return result

def idct_bloque(p,N):
	IDCT = getDCTInv(N)
	result = np.tensordot(IDCT, p, axes=[1,0])
	result = np.tensordot(IDCT, result.T, axes=[1,0])
	return result

"""
Reproducir los bloques base de la transformación para los casos N=4,8
Ver imágenes adjuntas.
"""
def drawBlocks(N):
	DCT = getDCT(N)
	fig = plt.figure()
	gs = gridspec.GridSpec(N, N)
	for i in range(0, N):
		for j in range(0, N):
			B = np.zeros( (N, N) )
			B[i][j] = 1
			B = idct_bloque(B,N)
			plt.subplot(N, N, (j * N) + i + 1)
			plt.imshow(B); plt.xticks([]) ; plt.yticks([]) ;
	plt.show()

drawBlocks(4)
drawBlocks(8)

###########################################################
# Algunas funciones utiles para mas adelante ##############
###########################################################

# Funciones para pasar de rgb a ycbcr y viceversa (he usado los coeficientes de Wikipedia)
def rgb2ycbcr(_img):
	img = _img.copy().astype(np.float)
	img[:,:,0] = ( 0.299     * _img[:,:,0]) + ( 0.587     * _img[:,:,1]) + ( 0.114    * _img[:,:,2]) + 0
	img[:,:,1] = (-0.168736  * _img[:,:,0]) + (-0.331263  * _img[:,:,1]) + ( 0.5      * _img[:,:,2]) + 128
	img[:,:,2] = ( 0.5       * _img[:,:,0]) + (-0.418688  * _img[:,:,1]) + (-0.081312 * _img[:,:,2]) + 128
	return img

# Funcion para pasar de ycbcr a rgb (he usado los coeficientes de Wikipedia)
def ycbcr2rgb(_img):
	img = _img.copy().astype(np.float)
	img[:,:,0] = _img[:,:,0]                                  + 1.402     * (_img[:,:,2] - 128)
	img[:,:,1] = _img[:,:,0] - 0.344136 * (_img[:,:,1] - 128) - 0.714136 * (_img[:,:,2] - 128)
	img[:,:,2] = _img[:,:,0] + 1.772    * (_img[:,:,1] - 128)
	return img
#############################################################

"""
Implementar la función jpeg_gris(imagen_gray) que: 
1. dibuje el resultado de aplicar la DCT y la cuantización 
(y sus inversas) a la imagen de grises 'imagen_gray' 

2. haga una estimación de la ratio de compresión
según los coeficientes nulos de la transformación: 
(#coeficientes/#coeficientes no nulos).

3. haga una estimación del error
Sigma=np.sqrt(sum(sum((imagen_gray-imagen_jpeg)**2)))/np.sqrt(sum(sum((imagen_gray)**2)))


En este caso optimizar la DCT 
http://docs.scipy.org/doc/numpy-1.10.1/reference/routines.linalg.html
"""

def jpeg_block(img_jpeg, bi, bj, channel, Q):
	B = (img_jpeg[bi*8:bi*8 + 8, bj*8:bj*8 + 8, channel])           # Obtenemos bloque
	B_DCT = dct_bloque(B, 8)                                        # Aplicamos DCT
	B_DCT_Q = np.round(B_DCT / Q) * Q
	B_DCT_Q_inv = idct_bloque(B_DCT_Q, 8)                           # Deshacemos DCT cuantizada
	img_jpeg[bi*8:bi*8 + 8, bj*8:bj*8 + 8, channel] = B_DCT_Q_inv   # Sustituimos bloque 8x8
	return np.count_nonzero(B_DCT_Q)


def jpeg_gris(img):
	
	# Convertir imagen a YCbCr
	img_jpeg = rgb2ycbcr(img) - 128 # Pasamos imagen a ycbcr y restamos 128
	
	# Aplicar DCT a bloques 8x8
	(NBh, NBw, _) = img_jpeg.shape
	NBw, NBh = NBw//8, NBh//8       # Obtenemos numero de bloques
	totalNonZeros = 0
	for bi in range(NBh):
		for bj in range(NBw):
			totalNonZeros += jpeg_block(img_jpeg, bi, bj, 0, Q_Luminance)
	
	# Reconvertir imagen a RGB
	img_jpeg = ycbcr2rgb(img_jpeg + 128)  # Reconvertimos la imagen a espacio RGB
	
	print ("Coeficientes no nulos = ", totalNonZeros)
	compressionRatio = ( (64.0 * NBw * NBh) / totalNonZeros )
	print ("Ratio de compresion estimado = ", compressionRatio)
	Sigma = np.sqrt(sum(sum((img[:,:,0] - img_jpeg[:,:,0])**2)))/np.sqrt(sum(sum((img[:,:,0])**2)))
	print ("Estimacion error: ", Sigma)

	plt.imshow(np.uint8(img_jpeg)); plt.show()
	
	return img_jpeg

"""
Implementar la función jpeg_color(imagen_color) que: 
1. dibuje el resultado de aplicar la DCT y la cuantización 
(y sus inversas) a la imagen RGB 'imagen_color' 

2. haga una estimación de la ratio de compresión
según los coeficientes nulos de la transformación: 
(#coeficientes/#coeficientes no nulos).

3. haga una estimación del error para cada una de las componentes RGB
Sigma=np.sqrt(sum(sum((imagen_color-imagen_jpeg)**2)))/np.sqrt(sum(sum((imagen_color)**2)))


En este caso optimizar la DCT 
http://docs.scipy.org/doc/numpy-1.10.1/reference/routines.linalg.html
"""


def jpeg_color(img):
	
	# Convertir imagen a YCbCr
	img_jpeg = rgb2ycbcr(img) - 128  # Pasamos imagen a ycbcr y restamos 128
	
	# Aplicar DCT a bloques 8x8
	(NBh, NBw, _) = img_jpeg.shape
	NBw, NBh = NBw//8, NBh//8       # Obtenemos numero de bloques
	totalNonZeros = 0
	for bi in range(NBh):
		for bj in range(NBw):
			totalNonZeros += jpeg_block(img_jpeg, bi, bj, 0, Q_Luminance)   # Y
			totalNonZeros += jpeg_block(img_jpeg, bi, bj, 1, Q_Chrominance) # Cb
			totalNonZeros += jpeg_block(img_jpeg, bi, bj, 2, Q_Chrominance) # Cr
	
	#Reconvertir imagen a RGB
	img_jpeg = ycbcr2rgb(img_jpeg + 128)  # Reconvertimos la imagen a espacio RGB
	
	print ("Coeficientes no nulos = ", totalNonZeros)
	compressionRatio = ( (64.0 * NBw * NBh * 3) / totalNonZeros )
	print ("Ratio de compresion estimado = ", compressionRatio)
	Sigma = np.sqrt(sum(sum((img - img_jpeg)**2)))/np.sqrt(sum(sum((img)**2)))
	print ("Estimacion error: ", Sigma)

	plt.imshow(np.uint8(img_jpeg)); plt.show()
	
	return img_jpeg

"""
#--------------------------------------------------------------------------
Imagen de GRISES
#--------------------------------------------------------------------------
"""


### .astype es para que lo lea como enteros de 32 bits, si no se
### pone lo lee como entero positivo sin signo de 8 bits uint8 y por ejemplo al 
### restar 128 puede devolver un valor positivo mayor que 128

# Cargamos imagen en color y pasamos a grises
mandril = scipy.ndimage.imread('mandril_gray.png').astype(np.int32)
mandril_gray = np.empty( (512,512,3) )
mandril_gray[:,:,0] = mandril_gray[:,:,1] = mandril_gray[:,:,2] = mandril[:,:] 
mandril_gray = mandril_gray.astype(np.int32)

#plt.imshow(mandril_gray); plt.show()
start = time.clock()
mandril_jpeg = jpeg_gris(mandril_gray)
end = time.clock()

print("tiempo", (end-start))
"""

#--------------------------------------------------------------------------
Imagen COLOR
#--------------------------------------------------------------------------
"""

## Aplico.astype pero después lo convertiré a 
## uint8 para dibujar y a int64 para calcular el error

mandril_color = scipy.misc.imread('./mandril_color.png').astype(np.float)
start = time.clock()
mandril_jpeg = jpeg_color(mandril_color)     
end = time.clock()
print("tiempo",(end-start))
       









