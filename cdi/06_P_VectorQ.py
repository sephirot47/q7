# -*- coding: utf-8 -*-
"""

"""

from scipy import misc
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
from scipy.cluster.vq import vq, kmeans

# FUNCTIONS

def rgb2gray(rgb): r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2] ; return 0.2989 * r + 0.5870 * g + 0.1140 * b

############

def GetTileBlocks(img, Width, Height, BlockSize):
	points = []
	for y in range(0, Height, BlockSize):
		for x in range(0, Width, BlockSize):
			point = []
			for dy in range(0, BlockSize):
				for dx in range(0, BlockSize):
					point.append(img[y+dy][x+dx])
			points.append(point)
	return np.array(points)

############
	
def GetTilesPalette(img, Width, Height, tileBlocks, NumPaletteTiles):

	print ("Doing K-Means...")
	tilesPalette, _ = kmeans(tileBlocks, NumPaletteTiles)
	print ("K-Means finished!")

	return tilesPalette

#############

def DrawImageWithTilePalette(img, Width, Height, BlockSize, tileBlocks, tilesPalette):
	
	NumPaletteTiles = len(tilesPalette)
	tilesPaletteIndices, _ = vq(tileBlocks, tilesPalette)
	kLena = [[0 for _ in range(0, Width)] for __ in range(0, Height)]
	for y in range(0, Height, BlockSize):
		for x in range(0, Width, BlockSize):
			idx   = tilesPaletteIndices[(y//BlockSize)*(Height/BlockSize) + x//BlockSize]
			pTile = tilesPalette[idx]
			for dy in range(0, BlockSize):
				for dx in range(0, BlockSize):
					pixelColor = pTile[dy*BlockSize + dx]
					kLena[y+dy][x+dx] = pixelColor

	error = np.sqrt( sum(  sum( (img - kLena)**2 )) ) / (Width * Height)

	bitsToSavePalette = NumPaletteTiles * (BlockSize**2) * 8
	bitsToSaveIndicesToPalette = (Width*Height) / (BlockSize**2) * int( np.log2(NumPaletteTiles) )
	compressedBits = bitsToSavePalette + bitsToSaveIndicesToPalette
	ratio = (Width * Height * 8) / compressedBits

	bitsPerPixel = compressedBits / (Width * Height)

	print ("Error:            ", error)
	print ("Ratio Compresion: ", ratio)
	print ("Bits per pixel:   ", bitsPerPixel)	

	plt.imshow(kLena, cmap = plt.cm.gray)
	plt.xticks([]) ; plt.yticks([]) ; plt.show()

##############

# PARAMETERS
BlockSize = 16
NumPaletteTiles = 32
##############

"""
Usando K-means http://docs.scipy.org/doc/scipy/reference/cluster.vq.html
crear un diccionario cuyas palabras sean bloques 8x8 con 512 entradas 
para la imagen de Lena.

Dibujar el resultado de codificar Lena con dicho diccionario.

Calcular el error, la ratio de compresión y el número de bits por píxel
"""
print ("")
print ("Lena: ")
img = misc.imread("lena_small.png")
img = rgb2gray(img) # Paso a grises, ya que uso una imagen en color...
tileBlocksLena   = GetTileBlocks(img, 128, 128, BlockSize)
tilesPaletteLena = GetTilesPalette(img, 128, 128, tileBlocksLena, NumPaletteTiles)
DrawImageWithTilePalette(img, 128, 128, BlockSize, tileBlocksLena, tilesPaletteLena)
print ("----------------------------------------")






"""
Hacer lo mismo con la imagen Peppers (escala de grises)
http://www.imageprocessingplace.com/downloads_V3/root_downloads/image_databases/standard_test_images.zip
"""
print ("Peppers: ")
img = misc.imread("peppers_gray.png")
tileBlocksPepper   = GetTileBlocks(img, 512, 512, BlockSize)
tilesPalettePepper = GetTilesPalette(img, 512, 512, tileBlocksPepper, NumPaletteTiles)
DrawImageWithTilePalette(img, 512, 512, BlockSize, tileBlocksPepper, tilesPalettePepper)
print ("----------------------------------------")






"""
Dibujar el resultado de codificar Peppers con el diccionarios obtenido
con la imagen de Lena.

Calcular el error.
"""
print ("Peppers con diccionario de Lena")
DrawImageWithTilePalette(img, 512, 512, BlockSize, tileBlocksPepper, tilesPaletteLena)
print ("----------------------------------------")



